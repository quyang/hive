package com.xianzaishi.wms.hive.domain.client.itf;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public interface IInventoryManageDomainClient {

	/**
	 * 插入流水信息，包括子流水信息
	 * @param inventoryManageVO
	 * @return
	 */
	public SimpleResultVO<Boolean> createInventoryManageDomain(
			InventoryManageVO inventoryManageVO);

	public SimpleResultVO<InventoryManageVO> getInventoryManageDomainByID(
			Long inventoryManageID);

	/**
	 * 更新流水信息，包括子流水信息
	 * @param inventoryManageVO
	 * @return
	 */
	public SimpleResultVO<InventoryManageVO> updateInventoryManageDomain(InventoryManageVO inventoryManageVO);



}
