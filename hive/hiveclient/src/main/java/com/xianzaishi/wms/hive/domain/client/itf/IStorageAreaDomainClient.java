package com.xianzaishi.wms.hive.domain.client.itf;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

public interface IStorageAreaDomainClient {

	public SimpleResultVO<Boolean> addStorageArea(StoryAreaVO storyAreaVO);

	public SimpleResultVO<StoryAreaVO> getStorageAreaByID(Long id);

	public SimpleResultVO<StoryAreaVO> getStorageAreaByCode(Long agencyID,
			String code);
}
