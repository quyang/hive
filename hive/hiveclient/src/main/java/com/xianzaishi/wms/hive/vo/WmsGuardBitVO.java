package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.BaseVO;

public class WmsGuardBitVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long skuId = null;

	private Integer guardBit = null;

	private Integer guardType = null;

	private Integer totalRecommond = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getGuardBit() {
		return guardBit;
	}

	public void setGuardBit(Integer guardBit) {
		this.guardBit = guardBit;
	}

	public Integer getGuardType() {
		return guardType;
	}

	public void setGuardType(Integer guardType) {
		this.guardType = guardType;
	}

	public Integer getTotalRecommond() {
		return totalRecommond;
	}

	public void setTotalRecommond(Integer totalRecommond) {
		this.totalRecommond = totalRecommond;
	}

}