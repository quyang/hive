package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class PositionDetailQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long slottingId = null;

	private Long positionId = null;

	private Long skuId = null;

	private Integer guardBit = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getGuardBit() {
		return guardBit;
	}

	public void setGuardBit(Integer guardBit) {
		this.guardBit = guardBit;
	}

}