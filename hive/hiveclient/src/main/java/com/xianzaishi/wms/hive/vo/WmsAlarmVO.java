package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class WmsAlarmVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long alarmType = null;

	private Date alertTime = null;

	private String content = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(Long alarmType) {
		this.alarmType = alarmType;
	}

	public Date getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(Date alertTime) {
		this.alertTime = alertTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}