package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InventoryLogVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long slottingId = null;

	private Long positionId = null;

	private Long skuId = null;

	private Short opType = null;

	private Integer opNumber = null;

	private Short opReason = null;

	private Date opTime = null;

	private Long operator = null;

	private String opContent = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Short getOpType() {
		return opType;
	}

	public void setOpType(Short opType) {
		this.opType = opType;
	}

	public Integer getOpNumber() {
		return opNumber;
	}

	public void setOpNumber(Integer opNumber) {
		this.opNumber = opNumber;
	}

	public Short getOpReason() {
		return opReason;
	}

	public void setOpReason(Short opReason) {
		this.opReason = opReason;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public String getOpContent() {
		return opContent;
	}

	public void setOpContent(String opContent) {
		this.opContent = opContent;
	}

}