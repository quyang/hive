package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class CollectSlottingQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long areaId = null;

	private String code = null;

	private Integer lineNo = null;

	private Integer columnNo = null;

	private String barcode = null;

	private Short slottingType = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public Integer getColumnNo() {
		return columnNo;
	}

	public void setColumnNo(Integer columnNo) {
		this.columnNo = columnNo;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Short getSlottingType() {
		return slottingType;
	}

	public void setSlottingType(Short slottingType) {
		this.slottingType = slottingType;
	}

}