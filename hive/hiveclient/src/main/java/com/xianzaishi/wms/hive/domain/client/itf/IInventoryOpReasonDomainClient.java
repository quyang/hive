package com.xianzaishi.wms.hive.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;

public interface IInventoryOpReasonDomainClient {

	public SimpleResultVO<List<InventoryOpReasonVO>> getInventoryOpReasonByOpType(
			Long opType);

}
