package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class AgencyQueryVO extends QueryVO implements Serializable {

	private String name = null;

	private Short type = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}
}