package com.xianzaishi.wms.hive.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public interface IInventoryConfigDomainClient {

	public SimpleResultVO<Boolean> delPositionDetail(Long id);

	public SimpleResultVO<Long> addPositionDetail(
			PositionDetailVO positionDetailVO);

	public SimpleResultVO<List<PositionDetailVO>> getPositionDetailBySkuID(
			Long agencyID, Long skuID);

	public SimpleResultVO<List<PositionDetailVO>> getPositionDetailByPositionID(
			Long agencyID, Long positionID);
}
