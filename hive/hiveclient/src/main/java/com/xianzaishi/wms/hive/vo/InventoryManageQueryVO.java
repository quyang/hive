package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class InventoryManageQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long operator = null;

	private Short opType = null;

	private Date opTime = null;

	private Short opReason = null;

	private Long auditor = null;

	private Date auditTime = null;

	private String comment = null;

	private Long relationId = null;

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Short getOpType() {
		return opType;
	}

	public void setOpType(Short opType) {
		this.opType = opType;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Short getOpReason() {
		return opReason;
	}

	public void setOpReason(Short opReason) {
		this.opReason = opReason;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}