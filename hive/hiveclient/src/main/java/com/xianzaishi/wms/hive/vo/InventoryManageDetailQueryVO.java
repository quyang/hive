package com.xianzaishi.wms.hive.vo;

import com.xianzaishi.wms.common.vo.QueryVO;
import java.io.Serializable;

public class InventoryManageDetailQueryVO extends QueryVO implements
		Serializable {

	private Long manageId = null;

	private Long positionId = null;

	private Long skuId = null;

	private Integer number = null;

	private Long relationId = null;

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public Long getManageId() {
		return manageId;
	}

	public void setManageId(Long manageId) {
		this.manageId = manageId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

}