package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InventoryVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long areaId = null;

	private Integer areaType = null;

	private Integer slottingType = null;

	private Long slottingId = null;

	private Long positionId = null;

	private String positionCode = null;

	private Long skuId = null;

	private String skuPlu = null;

	private Integer number = null;

	/**
	 * 真实库存乘以1000，by zhancang
	 */
	private Integer numberReal = null;

	private String numberShow = null;

	private Integer numberTotal = null;

	private Integer numberRealTotal = null;

	private String numberShowTotal = null;

	private Boolean maySale = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private String skuName = null;

	private Integer saleUnitType = null;

    /**
     * 安全库存字段
     */
	private String guardBit = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getSkuPlu() {
		return skuPlu;
	}

	public void setSkuPlu(String skuPlu) {
		this.skuPlu = skuPlu;
	}

	public Integer getNumber() {
		if (number == null && numberReal != null) {
			number = new BigDecimal(numberReal).divide(new BigDecimal(1000))
					.intValue();
		}
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	public Integer getSlottingType() {
		return slottingType;
	}

	public void setSlottingType(Integer slottingType) {
		this.slottingType = slottingType;
	}

	public Boolean getMaySale() {
		return maySale;
	}

	public void setMaySale(Boolean maySale) {
		this.maySale = maySale;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public Integer getNumberReal() {
		return numberReal;
	}

	public void setNumberReal(Integer numberReal) {
		this.numberReal = numberReal;
	}

	public String getNumberShow() {
		if (numberShow == null && numberReal != null) {
			numberShow = new BigDecimal(numberReal)
					.divide(new BigDecimal(1000)).toString();
		}
		return numberShow;
	}

	public void setNumberShow(String numberShow) {
		this.numberShow = numberShow;
	}

	public String getGuardBit() {
		return guardBit;
	}

	public void setGuardBit(String guardBit) {
		this.guardBit = guardBit;
	}

	public Integer getNumberTotal() {

		if (numberTotal == null && numberRealTotal != null) {
			numberTotal = new BigDecimal(numberRealTotal).divide(
					new BigDecimal(1000)).intValue();
		}
		return numberTotal;
	}

	public void setNumberTotal(Integer numberTotal) {
		this.numberTotal = numberTotal;
	}

	public Integer getNumberRealTotal() {
		return numberRealTotal;
	}

	public void setNumberRealTotal(Integer numberRealTotal) {
		this.numberRealTotal = numberRealTotal;
	}

	public String getNumberShowTotal() {
		if (numberShowTotal == null && numberRealTotal != null) {
			numberShowTotal = new BigDecimal(numberRealTotal).divide(
					new BigDecimal(1000)).toString();
		}
		return numberShowTotal;
	}

	public void setNumberShowTotal(String numberShowTotal) {
		this.numberShowTotal = numberShowTotal;
	}

}