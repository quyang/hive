package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class InventoryOpReasonQueryVO extends QueryVO implements Serializable {

	private Long opType = null;

	private String reasonName = null;

	public Long getOpType() {
		return opType;
	}

	public void setOpType(Long opType) {
		this.opType = opType;
	}

	public String getReasonName() {
		return reasonName;
	}

	public void setReasonName(String reasonName) {
		this.reasonName = reasonName;
	}

}