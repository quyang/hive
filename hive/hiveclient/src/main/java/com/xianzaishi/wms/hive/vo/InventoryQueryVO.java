package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.List;

import com.xianzaishi.wms.common.vo.QueryVO;

public class InventoryQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long areaId = null;

	private Long slottingId = null;

	private Integer areaType = null;

	private Integer slottingType = null;

	private Long positionId = null;

	/**
	 * 商品skuId
	 */
	private Long skuId = null;

	private List<Long> skuIds = null;

	private String skuPlu = null;

	private Integer number = null;

	/**
	 * 安全库存
	 */
	private String guardInventory;

	public String getGuardInventory() {
		return guardInventory;
	}

	public void setGuardInventory(String guardInventory) {
		this.guardInventory = guardInventory;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getSkuPlu() {
		return skuPlu;
	}

	public void setSkuPlu(String skuPlu) {
		this.skuPlu = skuPlu;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	public Integer getSlottingType() {
		return slottingType;
	}

	public void setSlottingType(Integer slottingType) {
		this.slottingType = slottingType;
	}

	public List<Long> getSkuIds() {
		return skuIds;
	}

	public void setSkuIds(List<Long> skuIds) {
		this.skuIds = skuIds;
	}

}