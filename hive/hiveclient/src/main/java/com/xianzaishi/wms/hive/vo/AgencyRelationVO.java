package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.BaseVO;

public class AgencyRelationVO extends BaseVO implements Serializable {

	private Long govId = null;

	private Long agencyId = null;

	public Long getGovId() {
		return govId;
	}

	public void setGovId(Long govId) {
		this.govId = govId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

}