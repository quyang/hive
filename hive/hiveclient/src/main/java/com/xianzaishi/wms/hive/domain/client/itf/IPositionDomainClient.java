package com.xianzaishi.wms.hive.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.PositionVO;

public interface IPositionDomainClient {

	public SimpleResultVO<PositionVO> getPositonByBarcode(String barcode);

	public SimpleResultVO<PositionVO> getPositonByCode(Long agencyID,
			String code);

	public SimpleResultVO<Boolean> addPosition(PositionVO positionVO);

	public SimpleResultVO<PositionVO> getPositionVOByID(Long id);

	public SimpleResultVO<List<PositionVO>> batchGetPositionVOByBarcode(
			List<String> barcodes);

	public SimpleResultVO<List<PositionVO>> batchGetPositionVOByID(
			List<Long> ids);
}
