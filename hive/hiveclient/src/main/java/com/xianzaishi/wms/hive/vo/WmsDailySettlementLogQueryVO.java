package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class WmsDailySettlementLogQueryVO extends QueryVO implements
		Serializable {

	private Long agencyId = null;

	private Integer opDay = null;

	private Short statu = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Integer getOpDay() {
		return opDay;
	}

	public void setOpDay(Integer opDay) {
		this.opDay = opDay;
	}

	public Short getStatu() {
		return statu;
	}

	public void setStatu(Short statu) {
		this.statu = statu;
	}

}