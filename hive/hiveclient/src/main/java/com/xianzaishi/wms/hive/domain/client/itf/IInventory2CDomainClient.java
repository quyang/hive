package com.xianzaishi.wms.hive.domain.client.itf;

import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public interface IInventory2CDomainClient {
	public SimpleResultVO<InventoryVO> getInventoryBySKUID(Long agencyID,
			Long skuID);

	public SimpleResultVO<List<InventoryVO>> batchGetInventoryBySKUID(
			Long agencyID, List<Long> skuID);

	public SimpleResultVO<List<InventoryVO>> batchGetInventoryByGovAndSKU(
			Long agencyID, List<Long> skuID);

	public SimpleResultVO<InventoryVO> getInventoryByGovAndSKU(Long govID,
			Long skuID);

	/**
	 * 设置安全库存
	 * @param skuId
	 * @param guradCount
	 * @return
	 */
	SimpleResultVO setGuardInventoryBySkuId(Long skuId, String guradCount);

	/**
	 * 插入安全库存信息
	 * @param data
	 * @return
	 */
	SimpleResultVO insettGuardInventoryInfo(WmsGuardBitQueryVO data);
}
