package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InventoryOpReasonVO extends BaseVO implements Serializable {

	private Short opType = null;

	private String reasonName = null;

	public Short getOpType() {
		return opType;
	}

	public void setOpType(Short opType) {
		this.opType = opType;
	}

	public String getReasonName() {
		return reasonName;
	}

	public void setReasonName(String reasonName) {
		this.reasonName = reasonName;
	}

}