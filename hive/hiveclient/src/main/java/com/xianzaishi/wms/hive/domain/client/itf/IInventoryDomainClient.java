package com.xianzaishi.wms.hive.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public interface IInventoryDomainClient {
	public SimpleResultVO<List<InventoryVO>> getInventoryBySKUID(Long agencyID,
			Long skuID);

	public SimpleResultVO<List<InventoryVO>> getInventoryByPositionID(
			Long agencyID, Long positionID, Integer pageSize, Integer pageNum);
	
	public SimpleResultVO<List<InventoryVO>> getAllInventoryBySKUID(
			Long agencyID, Long skuID);

	public SimpleResultVO<InventoryVO> getInventoryByPositionAndSku(
			Long positionID, Long skuID);

	public SimpleResultVO<Boolean> syncInventory(Long agencyID,
			Long positionID, Long skuID, Integer number);

	public SimpleResultVO<InventoryVO> getSkuStand(Long agencyID, Long skuID);
}
