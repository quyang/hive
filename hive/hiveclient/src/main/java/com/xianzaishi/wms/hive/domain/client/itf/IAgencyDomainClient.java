package com.xianzaishi.wms.hive.domain.client.itf;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.vo.AgencyVO;

public interface IAgencyDomainClient {
	public SimpleResultVO<AgencyVO> getAgencyByID(Long agencyID);
}
