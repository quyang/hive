package com.xianzaishi.wms.hive.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InventoryManageVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long operator = null;

	private Integer opType = null;

	private Date opTime = null;

	private Integer opReason = null;

	private Long auditor = null;

	private Date auditTime = null;

	private String comment = null;

	private List details = null;
	
	private Long inventoryId = null;

	private Long relationId = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Integer getOpType() {
		return opType;
	}

	public void setOpType(Integer opType) {
		this.opType = opType;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Integer getOpReason() {
		return opReason;
	}

	public void setOpReason(Integer opReason) {
		this.opReason = opReason;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List getDetails() {
		return details;
	}

	public void setDetails(List details) {
		this.details = details;
	}

    public Long getRelationId() {
      return relationId;
    }
  
    public void setRelationId(Long relationId) {
      this.relationId = relationId;
    }
  
    public Long getInventoryId() {
      return inventoryId;
    }
  
    public void setInventoryId(Long inventoryId) {
      this.inventoryId = inventoryId;
    }
	
}