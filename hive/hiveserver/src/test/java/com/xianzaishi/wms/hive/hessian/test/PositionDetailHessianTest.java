package com.xianzaishi.wms.hive.hessian.test;

import java.util.Random;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

@ContextConfiguration(locations = "classpath:spring/ac-hessian-hive.xml")
public class PositionDetailHessianTest extends BaseTest {
	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;

	@Test
	public void initPositionDetailTest() {
		Random random = new Random();
		for (int i = 10000; i <= 50000; i++) {
			PositionDetailVO positionDetailVO = new PositionDetailVO();
			positionDetailVO.setAgencyId(1l);
			positionDetailVO.setSkuId(new Long(i));
			positionDetailVO.setGuardBit(0);
			positionDetailVO.setPositionId(new Long(random.nextInt(361) + 25));
			inventoryConfigDomainClient.addPositionDetail(positionDetailVO);
		}
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}
}
