package com.xianzaishi.wms.hive.hessian.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IStorageAreaDomainClient;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

@ContextConfiguration(locations = "classpath:spring/ac-hessian-hive.xml")
public class PositionHessianTest extends BaseTest {

	@Autowired
	private IStorageAreaDomainClient storageAreaDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	@Test
	public void initInventoryTest() {
		StoryAreaVO storyAreaVO01 = storageAreaDomainClient
				.getStorageAreaByCode(1l, "QC01").getTarget();
		StoryAreaVO storyAreaVO02 = storageAreaDomainClient
				.getStorageAreaByCode(1l, "QC02").getTarget();
		StoryAreaVO storyAreaVO03 = storageAreaDomainClient
				.getStorageAreaByCode(1l, "QC03").getTarget();
		StoryAreaVO tmpStoryArea = null;
		int totalRow = 1;
		for (int i = 1; i <= 84; i++) {
			if (1 <= i && i <= 38) {
				tmpStoryArea = storyAreaVO01;
				if (1 <= i && i <= 3) {
					totalRow = 5;
				} else if (4 <= i && i <= 38) {
					totalRow = 7;
				}
			} else if ((39 <= i && i <= 63) || (83 <= i && i <= 84)) {
				tmpStoryArea = storyAreaVO02;
				if ((39 <= i && i <= 50) || (58 <= i && i <= 63)) {
					totalRow = 7;
				} else if (51 <= i && i <= 57) {
					totalRow = 4;
				} else if (83 <= i && i <= 84) {
					totalRow = 1;
				}
			} else if (64 <= i && i <= 82) {
				tmpStoryArea = storyAreaVO03;
				if (64 <= i && i <= 69) {
					totalRow = 7;
				} else if (i == 70) {
					totalRow = 2;
				} else if (71 <= i && i <= 82) {
					totalRow = 1;
				}
			}
			for (int j = 1; j <= totalRow; j++) {
				PositionVO positionVO = new PositionVO();
				positionVO.setAgencyId(1l);
				positionVO.setAreaId(tmpStoryArea.getId());
				positionVO.setCode(String.format("QC%03d%02d", i, j));
				positionDomainClient.addPosition(positionVO);
			}
		}

		StoryAreaVO hcStoryAreaVO = storageAreaDomainClient
				.getStorageAreaByCode(1l, "HC01").getTarget();
		for (int i = 1; i <= 12; i++) {
			PositionVO positionVO = new PositionVO();
			positionVO.setAgencyId(1l);
			positionVO.setAreaId(hcStoryAreaVO.getId());
			positionVO.setCode(String.format("HC%03d%02d", i, 1));
			positionDomainClient.addPosition(positionVO);
		}

		StoryAreaVO zcStoryAreaVO = storageAreaDomainClient
				.getStorageAreaByCode(1l, "ZC01").getTarget();
		for (int i = 1; i <= 20; i++) {
			PositionVO positionVO = new PositionVO();
			positionVO.setAgencyId(1l);
			positionVO.setAreaId(zcStoryAreaVO.getId());
			positionVO.setCode(String.format("HC%02d", i));
			positionDomainClient.addPosition(positionVO);
		}
	}

	public IStorageAreaDomainClient getStorageAreaDomainClient() {
		return storageAreaDomainClient;
	}

	public void setStorageAreaDomainClient(
			IStorageAreaDomainClient storageAreaDomainClient) {
		this.storageAreaDomainClient = storageAreaDomainClient;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
