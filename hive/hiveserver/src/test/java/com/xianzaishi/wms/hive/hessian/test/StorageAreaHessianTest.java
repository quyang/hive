package com.xianzaishi.wms.hive.hessian.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IStorageAreaDomainClient;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

@ContextConfiguration(locations = "classpath:spring/ac-hessian-hive.xml")
public class StorageAreaHessianTest extends BaseTest {

	@Autowired
	private IStorageAreaDomainClient storageAreaDomainClient = null;

	@Test
	public void initStorageAreaTest() {
		StoryAreaVO storyAreaVO = new StoryAreaVO();

		storyAreaVO = new StoryAreaVO();
		storyAreaVO.setAgencyId(1l);
		storyAreaVO.setAreaType(1);
		storyAreaVO.setCode("QC02");
		storyAreaVO.setName("前仓货架区02");
		storageAreaDomainClient.addStorageArea(storyAreaVO);

		storyAreaVO = new StoryAreaVO();
		storyAreaVO.setAgencyId(1l);
		storyAreaVO.setAreaType(1);
		storyAreaVO.setCode("QC03");
		storyAreaVO.setName("前仓货架区03");
		storageAreaDomainClient.addStorageArea(storyAreaVO);
	}

	public IStorageAreaDomainClient getStorageAreaDomainClient() {
		return storageAreaDomainClient;
	}

	public void setStorageAreaDomainClient(
			IStorageAreaDomainClient storageAreaDomainClient) {
		this.storageAreaDomainClient = storageAreaDomainClient;
	}
}
