package com.xianzaishi.wms.hive.hessian.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

@ContextConfiguration(locations = "classpath:spring/ac-hessian-hive.xml")
public class Inventory2CHessianTest extends BaseTest {

	@Autowired
	private IInventory2CDomainClient inventory2CDomainClient = null;

	@Test
	public void get2CInventoryTest() {
		System.out.println(inventory2CDomainClient.getInventoryBySKUID(1l,
				10046l).getTarget());
	}

	@Test
	public void batchGet2CInventoryTest() {
		List<Long> skuIds = new LinkedList<Long>();
		skuIds.add(10046l);
		skuIds.add(10661l);
		skuIds.add(20745l);
		List<InventoryVO> inventoryVOs = inventory2CDomainClient
				.batchGetInventoryBySKUID(1l, skuIds).getTarget();
		for (InventoryVO inventoryVO : inventoryVOs) {
			System.out.println(inventoryVO.getSkuId());
			System.out.println(inventoryVO.getMaySale());
			System.out.println(inventoryVO.getNumberShow());
			System.out.println(inventoryVO.getNumber());
			System.out.println(inventoryVO.getNumberReal());
		}
	}

	public IInventory2CDomainClient getInventory2CDomainClient() {
		return inventory2CDomainClient;
	}

	public void setInventory2CDomainClient(
			IInventory2CDomainClient inventory2cDomainClient) {
		inventory2CDomainClient = inventory2cDomainClient;
	}

}
