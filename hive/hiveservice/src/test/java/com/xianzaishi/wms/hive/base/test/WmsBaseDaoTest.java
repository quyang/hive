package com.xianzaishi.wms.hive.base.test;

import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;

@ContextConfiguration(locations = "classpath:spring/ac-hive-Datasource.xml")
public class WmsBaseDaoTest extends BaseTest {
}
