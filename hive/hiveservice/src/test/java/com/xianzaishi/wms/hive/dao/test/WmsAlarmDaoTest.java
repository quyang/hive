package com.xianzaishi.wms.hive.dao.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.hive.base.test.WmsBaseDaoTest;
import com.xianzaishi.wms.hive.dao.itf.IWmsAlarmDao;
import com.xianzaishi.wms.hive.vo.WmsAlarmDO;

@ContextConfiguration(locations = "classpath:spring/ac-hive-WmsAlarm.xml")
public class WmsAlarmDaoTest extends WmsBaseDaoTest {

	@Autowired
	private IWmsAlarmDao wmsAlarmDao = null;

	@Test
	public void addAlarmTest() {
		WmsAlarmDO wmsAlarmDO = new WmsAlarmDO();
		wmsAlarmDO.setAgencyId(1l);
		wmsAlarmDao.addDO(wmsAlarmDO);
	}
}
