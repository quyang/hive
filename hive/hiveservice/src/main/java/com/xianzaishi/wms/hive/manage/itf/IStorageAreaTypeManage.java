package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.StorageAreaTypeVO;
import com.xianzaishi.wms.hive.vo.StorageAreaTypeDO;
import com.xianzaishi.wms.hive.vo.StorageAreaTypeQueryVO;

public interface IStorageAreaTypeManage {

	public Boolean addStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO);

	public List<StorageAreaTypeVO> queryStorageAreaTypeVOList(StorageAreaTypeQueryVO storageAreaTypeQueryVO);

	public StorageAreaTypeVO getStorageAreaTypeVOByID(Long id);

	public Boolean modifyStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO);

	public Boolean deleteStorageAreaTypeVOByID(Long id);

}