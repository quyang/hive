package com.xianzaishi.wms.hive.domain.service.itf;

import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public interface IInventoryDomainService {
	public List<InventoryVO> getInventoryBySKUID(Long agencyID, Long skuID);

	public List<InventoryVO> getInventoryByPositionID(Long agencyID,
			Long positionID, Integer pageSize,Integer pageNum);

	public List<InventoryVO> getAllInventoryBySKUID(Long agencyID, Long skuID);

	public InventoryVO getInventoryByPositionAndSku(Long positionID, Long skuID);

	public void syncInventory(Long agency,
			InventoryManageDetailVO inventoryManageDetailVO);

	public InventoryVO get2CInventoryBySKUID(Long agencyID, Long skuID);

	public InventoryVO get2CInventoryByGovAndSKU(Long govID, Long skuID);

	public List<InventoryVO> batchGet2CInventoryBySKUID(Long agencyID,
			List<Long> skuID);

	public List<InventoryVO> batchGet2CInventoryByGovAndSKU(Long agencyID,
			List<Long> skuID);

	public InventoryVO getSkuStand(Long agencyID, Long skuID);

	/**
	 * 设置安全库存
	 * @param skuId
	 * @param guradCount
	 * @return
	 */
  Boolean setGuardInventoryBySkuId(Long skuId, String guradCount);

	/**
	 * 插入安全库存信息
	 * @param data
	 * @return
	 */
  Boolean insettGuardInventoryInfo(WmsGuardBitQueryVO data);
}
