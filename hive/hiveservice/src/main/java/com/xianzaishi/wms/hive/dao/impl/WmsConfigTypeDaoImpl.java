package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsConfigTypeDao;

public class WmsConfigTypeDaoImpl extends BaseDaoAdapter implements
		IWmsConfigTypeDao {
	public String getVOClassName() {
		return "WmsConfigTypeDO";
	}
}
