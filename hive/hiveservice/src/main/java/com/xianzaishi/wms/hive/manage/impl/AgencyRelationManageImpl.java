package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IAgencyRelationDao;
import com.xianzaishi.wms.hive.manage.itf.IAgencyRelationManage;
import com.xianzaishi.wms.hive.vo.AgencyRelationQueryVO;
import com.xianzaishi.wms.hive.vo.AgencyRelationVO;

public class AgencyRelationManageImpl implements IAgencyRelationManage {
	@Autowired
	private IAgencyRelationDao agencyRelationDao = null;

	private void validate(AgencyRelationVO agencyRelationVO) {
		if (agencyRelationVO.getAgencyId() == null
				|| agencyRelationVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ agencyRelationVO.getAgencyId());
		}
		if (agencyRelationVO.getGovId() == null
				|| agencyRelationVO.getGovId() <= 0) {
			throw new BizException("GovID error：" + agencyRelationVO.getGovId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IAgencyRelationDao getAgencyRelationDao() {
		return agencyRelationDao;
	}

	public void setAgencyRelationDao(IAgencyRelationDao agencyRelationDao) {
		this.agencyRelationDao = agencyRelationDao;
	}

	public Boolean addAgencyRelationVO(AgencyRelationVO agencyRelationVO) {
		validate(agencyRelationVO);
		agencyRelationDao.addDO(agencyRelationVO);
		return true;
	}

	public List<AgencyRelationVO> queryAgencyRelationVOList(
			AgencyRelationQueryVO agencyRelationQueryVO) {
		return agencyRelationDao.queryDO(agencyRelationQueryVO);
	}

	public AgencyRelationVO getAgencyRelationVOByID(Long id) {
		return (AgencyRelationVO) agencyRelationDao.getDOByID(id);
	}

	public Boolean modifyAgencyRelationVO(AgencyRelationVO agencyRelationVO) {
		return agencyRelationDao.updateDO(agencyRelationVO);
	}

	public Boolean deleteAgencyRelationVOByID(Long id) {
		return agencyRelationDao.delDO(id);
	}
}
