package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.CollectSlottingVO;
import com.xianzaishi.wms.hive.vo.CollectSlottingQueryVO;

public interface ICollectSlottingService {

	public Boolean addCollectSlottingVO(CollectSlottingVO collectSlottingVO);

	public List<CollectSlottingVO> queryCollectSlottingVOList(CollectSlottingQueryVO collectSlottingQueryVO);

	public CollectSlottingVO getCollectSlottingVOByID(Long id);

	public Boolean modifyCollectSlottingVO(CollectSlottingVO collectSlottingVO);

	public Boolean deleteCollectSlottingVOByID(Long id);

}