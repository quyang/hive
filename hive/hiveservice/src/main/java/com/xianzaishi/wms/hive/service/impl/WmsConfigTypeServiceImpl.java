package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsConfigTypeVO;
import com.xianzaishi.wms.hive.vo.WmsConfigTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsConfigTypeService;
import com.xianzaishi.wms.hive.manage.itf.IWmsConfigTypeManage;

public class WmsConfigTypeServiceImpl implements IWmsConfigTypeService {
	@Autowired
	private IWmsConfigTypeManage wmsConfigTypeManage = null;

	public IWmsConfigTypeManage getWmsConfigTypeManage() {
		return wmsConfigTypeManage;
	}

	public void setWmsConfigTypeManage(IWmsConfigTypeManage wmsConfigTypeManage) {
		this.wmsConfigTypeManage = wmsConfigTypeManage;
	}
	
	public Boolean addWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO) {
		wmsConfigTypeManage.addWmsConfigTypeVO(wmsConfigTypeVO);
		return true;
	}

	public List<WmsConfigTypeVO> queryWmsConfigTypeVOList(WmsConfigTypeQueryVO wmsConfigTypeQueryVO) {
		return wmsConfigTypeManage.queryWmsConfigTypeVOList(wmsConfigTypeQueryVO);
	}

	public WmsConfigTypeVO getWmsConfigTypeVOByID(Long id) {
		return (WmsConfigTypeVO) wmsConfigTypeManage.getWmsConfigTypeVOByID(id);
	}

	public Boolean modifyWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO) {
		return wmsConfigTypeManage.modifyWmsConfigTypeVO(wmsConfigTypeVO);
	}

	public Boolean deleteWmsConfigTypeVOByID(Long id) {
		return wmsConfigTypeManage.deleteWmsConfigTypeVOByID(id);
	}
}
