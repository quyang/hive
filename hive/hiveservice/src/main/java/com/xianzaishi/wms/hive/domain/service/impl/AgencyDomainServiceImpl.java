package com.xianzaishi.wms.hive.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.domain.service.itf.IAgencyDomainService;
import com.xianzaishi.wms.hive.service.itf.IAgencyService;
import com.xianzaishi.wms.hive.vo.AgencyVO;

public class AgencyDomainServiceImpl implements IAgencyDomainService {
	@Autowired
	private IAgencyService agencyService = null;

	public AgencyVO getAgencyByID(Long agencyID) {
		return agencyService.getAgencyVOByID(agencyID);
	}

	public IAgencyService getAgencyService() {
		return agencyService;
	}

	public void setAgencyService(IAgencyService agencyService) {
		this.agencyService = agencyService;
	}

}
