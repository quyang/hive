package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IInventoryManageDetailDao;

public class InventoryManageDetailDaoImpl extends BaseDaoAdapter implements
		IInventoryManageDetailDao {
	public String getVOClassName() {
		return "InventoryManageDetailDO";
	}
}
