package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IPositionDao;
import com.xianzaishi.wms.hive.manage.itf.IPositionManage;
import com.xianzaishi.wms.hive.vo.PositionQueryVO;
import com.xianzaishi.wms.hive.vo.PositionVO;

public class PositionManageImpl implements IPositionManage {
	@Autowired
	private IPositionDao positionDao = null;

	private void validate(PositionVO positionVO) {
		if (positionVO.getAgencyId() == null || positionVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + positionVO.getAgencyId());
		}
		if (positionVO.getBarcode() == null
				|| positionVO.getBarcode().isEmpty()) {
			throw new BizException("barcode error：" + positionVO.getBarcode());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IPositionDao getPositionDao() {
		return positionDao;
	}

	public void setPositionDao(IPositionDao positionDao) {
		this.positionDao = positionDao;
	}

	public Boolean addPositionVO(PositionVO positionVO) {
		validate(positionVO);
		positionDao.addDO(positionVO);
		return true;
	}

	public List<PositionVO> queryPositionVOList(PositionQueryVO positionQueryVO) {
		return positionDao.queryDO(positionQueryVO);
	}

	public PositionVO getPositionVOByID(Long id) {
		return (PositionVO) positionDao.getDOByID(id);
	}

	public Boolean modifyPositionVO(PositionVO positionVO) {
		return positionDao.updateDO(positionVO);
	}

	public Boolean deletePositionVOByID(Long id) {
		return positionDao.delDO(id);
	}
}
