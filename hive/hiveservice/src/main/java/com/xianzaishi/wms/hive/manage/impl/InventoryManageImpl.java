package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IInventoryDao;
import com.xianzaishi.wms.hive.manage.itf.IInventoryManage;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public class InventoryManageImpl implements IInventoryManage {
	@Autowired
	private IInventoryDao inventoryDao = null;

	private void validate(InventoryVO inventoryVO) {
		if (inventoryVO.getAgencyId() == null || inventoryVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ inventoryVO.getAgencyId());
		}
		if (inventoryVO.getAreaId() == null || inventoryVO.getAreaId() <= 0) {
			throw new BizException("areaID error：" + inventoryVO.getAreaId());
		}
		if (inventoryVO.getNumberReal() == null) {
			throw new BizException("number error："
					+ inventoryVO.getNumberReal());
		}
		if (inventoryVO.getPositionId() == null
				|| inventoryVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ inventoryVO.getPositionId());
		}
		if (inventoryVO.getSkuId() == null || inventoryVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + inventoryVO.getSkuId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInventoryDao getInventoryDao() {
		return inventoryDao;
	}

	public void setInventoryDao(IInventoryDao inventoryDao) {
		this.inventoryDao = inventoryDao;
	}

	public Boolean addInventoryVO(InventoryVO inventoryVO) {
		validate(inventoryVO);
		inventoryDao.addDO(inventoryVO);
		return true;
	}

	public List<InventoryVO> queryInventoryVOList(
			InventoryQueryVO inventoryQueryVO) {
		return inventoryDao.queryDO(inventoryQueryVO);
	}

	public InventoryVO getInventoryVOByID(Long id) {
		return (InventoryVO) inventoryDao.getDOByID(id);
	}

	public Boolean modifyInventoryVO(InventoryVO inventoryVO) {
		return inventoryDao.updateDO(inventoryVO);
	}

	public Boolean deleteInventoryVOByID(Long id) {
		return inventoryDao.deleteDO(id);
	}

	public Integer get2CInventory(Long agencyID, Long skuID) {
		return inventoryDao.get2CInventory(agencyID, skuID);
	}

	public List<InventoryVO> batchGet2CInventory(Long agencyID,
			List<Long> skuIDs) {
		return inventoryDao.batchGet2CInventory(agencyID, skuIDs);
	}
}
