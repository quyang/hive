package com.xianzaishi.wms.hive.domain.client.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IStorageAreaDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IStorageAreaDomainService;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

public class StorageAreaDomainClient implements IStorageAreaDomainClient {
	@Autowired
	private IStorageAreaDomainService storageAreaDomainService = null;

	@Override
	public SimpleResultVO<Boolean> addStorageArea(StoryAreaVO storyAreaVO) {
		return SimpleResultVO.buildSuccessResult(storageAreaDomainService
				.addStorageArea(storyAreaVO));
	}

	public IStorageAreaDomainService getStorageAreaDomainService() {
		return storageAreaDomainService;
	}

	public void setStorageAreaDomainService(
			IStorageAreaDomainService storageAreaDomainService) {
		this.storageAreaDomainService = storageAreaDomainService;
	}

	public SimpleResultVO<StoryAreaVO> getStorageAreaByID(Long id) {
		return SimpleResultVO.buildSuccessResult(storageAreaDomainService
				.getStorageAreaByID(id));
	}

	public SimpleResultVO<StoryAreaVO> getStorageAreaByCode(Long agencyID,
			String code) {
		return SimpleResultVO.buildSuccessResult(storageAreaDomainService
				.getStorageAreaByCode(agencyID, code));
	}

}
