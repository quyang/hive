package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IInventoryManageDao;
import com.xianzaishi.wms.hive.manage.itf.IInventoryManageManage;
import com.xianzaishi.wms.hive.vo.InventoryManageQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public class InventoryManageManageImpl implements IInventoryManageManage {
	@Autowired
	private IInventoryManageDao inventoryManageDao = null;

	private void validate(InventoryManageVO inventoryManageVO) {
		if (inventoryManageVO.getAgencyId() == null
				|| inventoryManageVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ inventoryManageVO.getAgencyId());
		}
		if (inventoryManageVO.getOpReason() == null
				|| inventoryManageVO.getOpReason() <= 0) {
			throw new BizException("opReason error："
					+ inventoryManageVO.getOpReason());
		}
		if (inventoryManageVO.getOpType() == null
				|| inventoryManageVO.getOpType() <= 0) {
			throw new BizException("opType error："
					+ inventoryManageVO.getOpType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInventoryManageDao getInventoryManageDao() {
		return inventoryManageDao;
	}

	public void setInventoryManageDao(IInventoryManageDao inventoryManageDao) {
		this.inventoryManageDao = inventoryManageDao;
	}

	public Long addInventoryManageVO(InventoryManageVO inventoryManageVO) {
		validate(inventoryManageVO);
		return (Long) inventoryManageDao.addDO(inventoryManageVO);
	}

	public List<InventoryManageVO> queryInventoryManageVOList(
			InventoryManageQueryVO inventoryManageQueryVO) {
		return inventoryManageDao.queryDO(inventoryManageQueryVO);
	}

	public InventoryManageVO getInventoryManageVOByID(Long id) {
		return (InventoryManageVO) inventoryManageDao.getDOByID(id);
	}

	public Boolean modifyInventoryManageVO(InventoryManageVO inventoryManageVO) {
		return inventoryManageDao.updateDO(inventoryManageVO);
	}

	public Boolean deleteInventoryManageVOByID(Long id) {
		return inventoryManageDao.delDO(id);
	}
}
