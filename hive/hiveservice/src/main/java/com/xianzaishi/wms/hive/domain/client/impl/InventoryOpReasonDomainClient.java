package com.xianzaishi.wms.hive.domain.client.impl;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryOpReasonDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryOpReasonDomainService;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryOpReasonDomainClient implements
		IInventoryOpReasonDomainClient {
	private static final Logger logger = Logger
			.getLogger(InventoryOpReasonDomainClient.class);
	@Autowired
	private IInventoryOpReasonDomainService inventoryOpReasonDomainService = null;

	public SimpleResultVO<List<InventoryOpReasonVO>> getInventoryOpReasonByOpType(
			Long opType) {
		SimpleResultVO<List<InventoryOpReasonVO>> flag = null;
		try {
			logger.error("参数opType=" + opType);
			flag = SimpleResultVO
					.buildSuccessResult(inventoryOpReasonDomainService
							.getInventoryOpReasonByOpType(opType));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public IInventoryOpReasonDomainService getInventoryOpReasonDomainService() {
		return inventoryOpReasonDomainService;
	}

	public void setInventoryOpReasonDomainService(
			IInventoryOpReasonDomainService inventoryOpReasonDomainService) {
		this.inventoryOpReasonDomainService = inventoryOpReasonDomainService;
	}

}
