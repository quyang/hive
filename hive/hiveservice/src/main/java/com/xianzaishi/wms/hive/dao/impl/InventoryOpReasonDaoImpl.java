package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IInventoryOpReasonDao;

public class InventoryOpReasonDaoImpl extends BaseDaoAdapter implements
		IInventoryOpReasonDao {
	public String getVOClassName() {
		return "InventoryOpReasonDO";
	}
}
