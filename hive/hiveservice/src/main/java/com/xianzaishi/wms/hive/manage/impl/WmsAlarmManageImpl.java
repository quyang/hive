package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsAlarmDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsAlarmManage;
import com.xianzaishi.wms.hive.vo.WmsAlarmQueryVO;
import com.xianzaishi.wms.hive.vo.WmsAlarmVO;

public class WmsAlarmManageImpl implements IWmsAlarmManage {
	@Autowired
	private IWmsAlarmDao wmsAlarmDao = null;

	private void validate(WmsAlarmVO wmsAlarmVO) {
		if (wmsAlarmVO.getAgencyId() == null || wmsAlarmVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + wmsAlarmVO.getAgencyId());
		}
		if (wmsAlarmVO.getAlarmType() == null || wmsAlarmVO.getAlarmType() <= 0) {
			throw new BizException("type error：" + wmsAlarmVO.getAlarmType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsAlarmDao getWmsAlarmDao() {
		return wmsAlarmDao;
	}

	public void setWmsAlarmDao(IWmsAlarmDao wmsAlarmDao) {
		this.wmsAlarmDao = wmsAlarmDao;
	}

	public Boolean addWmsAlarmVO(WmsAlarmVO wmsAlarmVO) {
		validate(wmsAlarmVO);
		wmsAlarmDao.addDO(wmsAlarmVO);
		return true;
	}

	public List<WmsAlarmVO> queryWmsAlarmVOList(WmsAlarmQueryVO wmsAlarmQueryVO) {
		return wmsAlarmDao.queryDO(wmsAlarmQueryVO);
	}

	public WmsAlarmVO getWmsAlarmVOByID(Long id) {
		return (WmsAlarmVO) wmsAlarmDao.getDOByID(id);
	}

	public Boolean modifyWmsAlarmVO(WmsAlarmVO wmsAlarmVO) {
		return wmsAlarmDao.updateDO(wmsAlarmVO);
	}

	public Boolean deleteWmsAlarmVOByID(Long id) {
		return wmsAlarmDao.delDO(id);
	}
}
