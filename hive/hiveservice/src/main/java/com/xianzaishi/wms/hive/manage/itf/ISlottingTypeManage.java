package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.SlottingTypeVO;
import com.xianzaishi.wms.hive.vo.SlottingTypeDO;
import com.xianzaishi.wms.hive.vo.SlottingTypeQueryVO;

public interface ISlottingTypeManage {

	public Boolean addSlottingTypeVO(SlottingTypeVO slottingTypeVO);

	public List<SlottingTypeVO> querySlottingTypeVOList(SlottingTypeQueryVO slottingTypeQueryVO);

	public SlottingTypeVO getSlottingTypeVOByID(Long id);

	public Boolean modifySlottingTypeVO(SlottingTypeVO slottingTypeVO);

	public Boolean deleteSlottingTypeVOByID(Long id);

}