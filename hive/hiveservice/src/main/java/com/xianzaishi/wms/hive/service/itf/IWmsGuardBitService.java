package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsGuardBitVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;

public interface IWmsGuardBitService {

	public Boolean addWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO);

	public List<WmsGuardBitVO> queryWmsGuardBitVOList(WmsGuardBitQueryVO wmsGuardBitQueryVO);

	public WmsGuardBitVO getWmsGuardBitVOByID(Long id);

	public Boolean modifyWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO);

	public Boolean deleteWmsGuardBitVOByID(Long id);

}