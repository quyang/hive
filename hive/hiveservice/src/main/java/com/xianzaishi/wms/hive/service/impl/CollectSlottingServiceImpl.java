package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.CollectSlottingVO;
import com.xianzaishi.wms.hive.vo.CollectSlottingQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.ICollectSlottingService;
import com.xianzaishi.wms.hive.manage.itf.ICollectSlottingManage;

public class CollectSlottingServiceImpl implements ICollectSlottingService {
	@Autowired
	private ICollectSlottingManage collectSlottingManage = null;

	public ICollectSlottingManage getCollectSlottingManage() {
		return collectSlottingManage;
	}

	public void setCollectSlottingManage(ICollectSlottingManage collectSlottingManage) {
		this.collectSlottingManage = collectSlottingManage;
	}
	
	public Boolean addCollectSlottingVO(CollectSlottingVO collectSlottingVO) {
		collectSlottingManage.addCollectSlottingVO(collectSlottingVO);
		return true;
	}

	public List<CollectSlottingVO> queryCollectSlottingVOList(CollectSlottingQueryVO collectSlottingQueryVO) {
		return collectSlottingManage.queryCollectSlottingVOList(collectSlottingQueryVO);
	}

	public CollectSlottingVO getCollectSlottingVOByID(Long id) {
		return (CollectSlottingVO) collectSlottingManage.getCollectSlottingVOByID(id);
	}

	public Boolean modifyCollectSlottingVO(CollectSlottingVO collectSlottingVO) {
		return collectSlottingManage.modifyCollectSlottingVO(collectSlottingVO);
	}

	public Boolean deleteCollectSlottingVOByID(Long id) {
		return collectSlottingManage.deleteCollectSlottingVOByID(id);
	}
}
