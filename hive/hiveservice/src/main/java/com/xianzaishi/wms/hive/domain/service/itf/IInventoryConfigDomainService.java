package com.xianzaishi.wms.hive.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public interface IInventoryConfigDomainService {
	public Boolean delPositionDetail(Long id);

	public Long addPositionDetail(PositionDetailVO positionDetailVO);

	public List<PositionDetailVO> getPositionDetailBySkuID(Long agencyID,
			Long skuID);

	public List<PositionDetailVO> getPositionDetailByPositionID(Long agencyID,
			Long positionID);
}
