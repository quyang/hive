package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.StorageAreaTypeVO;
import com.xianzaishi.wms.hive.vo.StorageAreaTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IStorageAreaTypeService;
import com.xianzaishi.wms.hive.manage.itf.IStorageAreaTypeManage;

public class StorageAreaTypeServiceImpl implements IStorageAreaTypeService {
	@Autowired
	private IStorageAreaTypeManage storageAreaTypeManage = null;

	public IStorageAreaTypeManage getStorageAreaTypeManage() {
		return storageAreaTypeManage;
	}

	public void setStorageAreaTypeManage(IStorageAreaTypeManage storageAreaTypeManage) {
		this.storageAreaTypeManage = storageAreaTypeManage;
	}
	
	public Boolean addStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO) {
		storageAreaTypeManage.addStorageAreaTypeVO(storageAreaTypeVO);
		return true;
	}

	public List<StorageAreaTypeVO> queryStorageAreaTypeVOList(StorageAreaTypeQueryVO storageAreaTypeQueryVO) {
		return storageAreaTypeManage.queryStorageAreaTypeVOList(storageAreaTypeQueryVO);
	}

	public StorageAreaTypeVO getStorageAreaTypeVOByID(Long id) {
		return (StorageAreaTypeVO) storageAreaTypeManage.getStorageAreaTypeVOByID(id);
	}

	public Boolean modifyStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO) {
		return storageAreaTypeManage.modifyStorageAreaTypeVO(storageAreaTypeVO);
	}

	public Boolean deleteStorageAreaTypeVOByID(Long id) {
		return storageAreaTypeManage.deleteStorageAreaTypeVOByID(id);
	}
}
