package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.AgencyRelationVO;
import com.xianzaishi.wms.hive.vo.AgencyRelationDO;
import com.xianzaishi.wms.hive.vo.AgencyRelationQueryVO;

public interface IAgencyRelationManage {

	public Boolean addAgencyRelationVO(AgencyRelationVO agencyRelationVO);

	public List<AgencyRelationVO> queryAgencyRelationVOList(AgencyRelationQueryVO agencyRelationQueryVO);

	public AgencyRelationVO getAgencyRelationVOByID(Long id);

	public Boolean modifyAgencyRelationVO(AgencyRelationVO agencyRelationVO);

	public Boolean deleteAgencyRelationVOByID(Long id);

}