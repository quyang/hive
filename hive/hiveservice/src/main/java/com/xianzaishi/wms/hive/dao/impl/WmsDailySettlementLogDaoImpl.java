package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsDailySettlementLogDao;

public class WmsDailySettlementLogDaoImpl extends BaseDaoAdapter implements
		IWmsDailySettlementLogDao {
	public String getVOClassName() {
		return "WmsDailySettlementLogDO";
	}
}
