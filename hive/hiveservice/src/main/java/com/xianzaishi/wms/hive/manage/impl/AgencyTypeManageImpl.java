package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IAgencyTypeDao;
import com.xianzaishi.wms.hive.manage.itf.IAgencyTypeManage;
import com.xianzaishi.wms.hive.vo.AgencyTypeQueryVO;
import com.xianzaishi.wms.hive.vo.AgencyTypeVO;

public class AgencyTypeManageImpl implements IAgencyTypeManage {
	@Autowired
	private IAgencyTypeDao agencyTypeDao = null;

	private void validate(AgencyTypeVO agencyTypeVO) {
		if (agencyTypeVO.getName() == null || agencyTypeVO.getName().isEmpty()) {
			throw new BizException("name error：" + agencyTypeVO.getName());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IAgencyTypeDao getAgencyTypeDao() {
		return agencyTypeDao;
	}

	public void setAgencyTypeDao(IAgencyTypeDao agencyTypeDao) {
		this.agencyTypeDao = agencyTypeDao;
	}

	public Boolean addAgencyTypeVO(AgencyTypeVO agencyTypeVO) {
		validate(agencyTypeVO);
		agencyTypeDao.addDO(agencyTypeVO);
		return true;
	}

	public List<AgencyTypeVO> queryAgencyTypeVOList(
			AgencyTypeQueryVO agencyTypeQueryVO) {
		return agencyTypeDao.queryDO(agencyTypeQueryVO);
	}

	public AgencyTypeVO getAgencyTypeVOByID(Long id) {
		return (AgencyTypeVO) agencyTypeDao.getDOByID(id);
	}

	public Boolean modifyAgencyTypeVO(AgencyTypeVO agencyTypeVO) {
		return agencyTypeDao.updateDO(agencyTypeVO);
	}

	public Boolean deleteAgencyTypeVOByID(Long id) {
		return agencyTypeDao.delDO(id);
	}
}
