package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public interface IPositionDetailManage {

	public Long addPositionDetailVO(PositionDetailVO positionDetailVO);

	public List<PositionDetailVO> queryPositionDetailVOList(
			PositionDetailQueryVO positionDetailQueryVO);

	public PositionDetailVO getPositionDetailVOByID(Long id);

	public Boolean modifyPositionDetailVO(PositionDetailVO positionDetailVO);

	public Boolean deletePositionDetailVOByID(Long id);

}