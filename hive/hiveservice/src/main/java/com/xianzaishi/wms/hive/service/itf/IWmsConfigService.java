package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsConfigVO;
import com.xianzaishi.wms.hive.vo.WmsConfigQueryVO;

public interface IWmsConfigService {

	public Boolean addWmsConfigVO(WmsConfigVO wmsConfigVO);

	public List<WmsConfigVO> queryWmsConfigVOList(WmsConfigQueryVO wmsConfigQueryVO);

	public WmsConfigVO getWmsConfigVOByID(Long id);

	public Boolean modifyWmsConfigVO(WmsConfigVO wmsConfigVO);

	public Boolean deleteWmsConfigVOByID(Long id);

}