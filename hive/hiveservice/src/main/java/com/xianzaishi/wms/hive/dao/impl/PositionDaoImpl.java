package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IPositionDao;

public class PositionDaoImpl extends BaseDaoAdapter implements IPositionDao {
	public String getVOClassName() {
		return "PositionDO";
	}
}
