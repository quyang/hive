package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.AgencyTypeVO;
import com.xianzaishi.wms.hive.vo.AgencyTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IAgencyTypeService;
import com.xianzaishi.wms.hive.manage.itf.IAgencyTypeManage;

public class AgencyTypeServiceImpl implements IAgencyTypeService {
	@Autowired
	private IAgencyTypeManage agencyTypeManage = null;

	public IAgencyTypeManage getAgencyTypeManage() {
		return agencyTypeManage;
	}

	public void setAgencyTypeManage(IAgencyTypeManage agencyTypeManage) {
		this.agencyTypeManage = agencyTypeManage;
	}
	
	public Boolean addAgencyTypeVO(AgencyTypeVO agencyTypeVO) {
		agencyTypeManage.addAgencyTypeVO(agencyTypeVO);
		return true;
	}

	public List<AgencyTypeVO> queryAgencyTypeVOList(AgencyTypeQueryVO agencyTypeQueryVO) {
		return agencyTypeManage.queryAgencyTypeVOList(agencyTypeQueryVO);
	}

	public AgencyTypeVO getAgencyTypeVOByID(Long id) {
		return (AgencyTypeVO) agencyTypeManage.getAgencyTypeVOByID(id);
	}

	public Boolean modifyAgencyTypeVO(AgencyTypeVO agencyTypeVO) {
		return agencyTypeManage.modifyAgencyTypeVO(agencyTypeVO);
	}

	public Boolean deleteAgencyTypeVOByID(Long id) {
		return agencyTypeManage.deleteAgencyTypeVOByID(id);
	}
}
