package com.xianzaishi.wms.hive.domain.client.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryManageDomainService;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public class InventoryManageDomainClient implements
		IInventoryManageDomainClient {
	private static final Logger logger = Logger
			.getLogger(InventoryManageDomainClient.class);
	@Autowired
	private IInventoryManageDomainService inventoryManageDomainService = null;

	public SimpleResultVO<Boolean> createInventoryManageDomain(
			InventoryManageVO inventoryManageVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(inventoryManageDomainService
							.createInventoryManageDomain(inventoryManageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<InventoryManageVO> getInventoryManageDomainByID(
			Long inventoryManageID) {
		SimpleResultVO<InventoryManageVO> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(inventoryManageDomainService
							.getInventoryManageDomainByID(inventoryManageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	@Override
	public SimpleResultVO<InventoryManageVO> updateInventoryManageDomain(InventoryManageVO inventoryManageVO) {
		SimpleResultVO<InventoryManageVO> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(inventoryManageDomainService
							.updateInventoryManageDomain(inventoryManageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public IInventoryManageDomainService getInventoryManageDomainService() {
		return inventoryManageDomainService;
	}

	public void setInventoryManageDomainService(
			IInventoryManageDomainService inventoryManageDomainService) {
		this.inventoryManageDomainService = inventoryManageDomainService;
	}

}
