package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.AgencyVO;
import com.xianzaishi.wms.hive.vo.AgencyQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IAgencyService;
import com.xianzaishi.wms.hive.manage.itf.IAgencyManage;

public class AgencyServiceImpl implements IAgencyService {
	@Autowired
	private IAgencyManage agencyManage = null;

	public IAgencyManage getAgencyManage() {
		return agencyManage;
	}

	public void setAgencyManage(IAgencyManage agencyManage) {
		this.agencyManage = agencyManage;
	}
	
	public Boolean addAgencyVO(AgencyVO agencyVO) {
		agencyManage.addAgencyVO(agencyVO);
		return true;
	}

	public List<AgencyVO> queryAgencyVOList(AgencyQueryVO agencyQueryVO) {
		return agencyManage.queryAgencyVOList(agencyQueryVO);
	}

	public AgencyVO getAgencyVOByID(Long id) {
		return (AgencyVO) agencyManage.getAgencyVOByID(id);
	}

	public Boolean modifyAgencyVO(AgencyVO agencyVO) {
		return agencyManage.modifyAgencyVO(agencyVO);
	}

	public Boolean deleteAgencyVOByID(Long id) {
		return agencyManage.deleteAgencyVOByID(id);
	}
}
