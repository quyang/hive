package com.xianzaishi.wms.hive.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryConfigDomainService;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public class InventoryConfigDomainClient implements
		IInventoryConfigDomainClient {

	@Autowired
	private IInventoryConfigDomainService inventoryConfigDomainService = null;

	public SimpleResultVO<Long> addPositionDetail(
			PositionDetailVO positionDetailVO) {
		return SimpleResultVO.buildSuccessResult(inventoryConfigDomainService
				.addPositionDetail(positionDetailVO));
	}

	public SimpleResultVO<List<PositionDetailVO>> getPositionDetailBySkuID(
			Long agencyID, Long skuID) {
		return SimpleResultVO.buildSuccessResult(inventoryConfigDomainService
				.getPositionDetailBySkuID(agencyID, skuID));
	}

	public SimpleResultVO<List<PositionDetailVO>> getPositionDetailByPositionID(
			Long agencyID, Long positionID) {
		return SimpleResultVO.buildSuccessResult(inventoryConfigDomainService
				.getPositionDetailByPositionID(agencyID, positionID));
	}

	public IInventoryConfigDomainService getInventoryConfigDomainService() {
		return inventoryConfigDomainService;
	}

	public void setInventoryConfigDomainService(
			IInventoryConfigDomainService inventoryConfigDomainService) {
		this.inventoryConfigDomainService = inventoryConfigDomainService;
	}

	@Override
	public SimpleResultVO<Boolean> delPositionDetail(Long id) {
		return SimpleResultVO.buildSuccessResult(inventoryConfigDomainService
				.delPositionDetail(id));
	}
}
