package com.xianzaishi.wms.hive.domain.service.itf;

import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public interface IPositionDetailDomainService {

	public Boolean addPositionDetail(PositionDetailVO positionDetailVO);

	public PositionDetailVO getPositionDetailBySkuID(Long agencyID, Long skuID);

}
