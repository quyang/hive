package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsGuardBitDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsGuardBitManage;
import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitVO;

public class WmsGuardBitManageImpl implements IWmsGuardBitManage {
	@Autowired
	private IWmsGuardBitDao wmsGuardBitDao = null;

	private void validate(WmsGuardBitVO wmsGuardBitVO) {
		if (wmsGuardBitVO.getAgencyId() == null
				|| wmsGuardBitVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ wmsGuardBitVO.getAgencyId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsGuardBitDao getWmsGuardBitDao() {
		return wmsGuardBitDao;
	}

	public void setWmsGuardBitDao(IWmsGuardBitDao wmsGuardBitDao) {
		this.wmsGuardBitDao = wmsGuardBitDao;
	}

	public Boolean addWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO) {
		validate(wmsGuardBitVO);
		wmsGuardBitDao.addDO(wmsGuardBitVO);
		return true;
	}

	public List<WmsGuardBitVO> queryWmsGuardBitVOList(
			WmsGuardBitQueryVO wmsGuardBitQueryVO) {
		return wmsGuardBitDao.queryDO(wmsGuardBitQueryVO);
	}

	public WmsGuardBitVO getWmsGuardBitVOByID(Long id) {
		return (WmsGuardBitVO) wmsGuardBitDao.getDOByID(id);
	}

	public Boolean modifyWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO) {
		return wmsGuardBitDao.updateDO(wmsGuardBitVO);
	}

	public Boolean deleteWmsGuardBitVOByID(Long id) {
		return wmsGuardBitDao.delDO(id);
	}
}
