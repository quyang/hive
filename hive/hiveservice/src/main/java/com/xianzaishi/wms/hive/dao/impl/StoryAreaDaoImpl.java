package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IStoryAreaDao;

public class StoryAreaDaoImpl extends BaseDaoAdapter implements IStoryAreaDao {
	public String getVOClassName() {
		return "StoryAreaDO";
	}
}
