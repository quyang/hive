package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IStorageAreaTypeDao;
import com.xianzaishi.wms.hive.manage.itf.IStorageAreaTypeManage;
import com.xianzaishi.wms.hive.vo.StorageAreaTypeQueryVO;
import com.xianzaishi.wms.hive.vo.StorageAreaTypeVO;

public class StorageAreaTypeManageImpl implements IStorageAreaTypeManage {
	@Autowired
	private IStorageAreaTypeDao storageAreaTypeDao = null;

	private void validate(StorageAreaTypeVO storageAreaTypeVO) {
		if (storageAreaTypeVO.getName() == null
				|| storageAreaTypeVO.getName().isEmpty()) {
			throw new BizException("name error：" + storageAreaTypeVO.getName());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStorageAreaTypeDao getStorageAreaTypeDao() {
		return storageAreaTypeDao;
	}

	public void setStorageAreaTypeDao(IStorageAreaTypeDao storageAreaTypeDao) {
		this.storageAreaTypeDao = storageAreaTypeDao;
	}

	public Boolean addStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO) {
		validate(storageAreaTypeVO);
		storageAreaTypeDao.addDO(storageAreaTypeVO);
		return true;
	}

	public List<StorageAreaTypeVO> queryStorageAreaTypeVOList(
			StorageAreaTypeQueryVO storageAreaTypeQueryVO) {
		return storageAreaTypeDao.queryDO(storageAreaTypeQueryVO);
	}

	public StorageAreaTypeVO getStorageAreaTypeVOByID(Long id) {
		return (StorageAreaTypeVO) storageAreaTypeDao.getDOByID(id);
	}

	public Boolean modifyStorageAreaTypeVO(StorageAreaTypeVO storageAreaTypeVO) {
		return storageAreaTypeDao.updateDO(storageAreaTypeVO);
	}

	public Boolean deleteStorageAreaTypeVOByID(Long id) {
		return storageAreaTypeDao.delDO(id);
	}
}
