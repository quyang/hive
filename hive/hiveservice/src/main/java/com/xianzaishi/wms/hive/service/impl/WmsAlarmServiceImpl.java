package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsAlarmVO;
import com.xianzaishi.wms.hive.vo.WmsAlarmQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsAlarmService;
import com.xianzaishi.wms.hive.manage.itf.IWmsAlarmManage;

public class WmsAlarmServiceImpl implements IWmsAlarmService {
	@Autowired
	private IWmsAlarmManage wmsAlarmManage = null;

	public IWmsAlarmManage getWmsAlarmManage() {
		return wmsAlarmManage;
	}

	public void setWmsAlarmManage(IWmsAlarmManage wmsAlarmManage) {
		this.wmsAlarmManage = wmsAlarmManage;
	}
	
	public Boolean addWmsAlarmVO(WmsAlarmVO wmsAlarmVO) {
		wmsAlarmManage.addWmsAlarmVO(wmsAlarmVO);
		return true;
	}

	public List<WmsAlarmVO> queryWmsAlarmVOList(WmsAlarmQueryVO wmsAlarmQueryVO) {
		return wmsAlarmManage.queryWmsAlarmVOList(wmsAlarmQueryVO);
	}

	public WmsAlarmVO getWmsAlarmVOByID(Long id) {
		return (WmsAlarmVO) wmsAlarmManage.getWmsAlarmVOByID(id);
	}

	public Boolean modifyWmsAlarmVO(WmsAlarmVO wmsAlarmVO) {
		return wmsAlarmManage.modifyWmsAlarmVO(wmsAlarmVO);
	}

	public Boolean deleteWmsAlarmVOByID(Long id) {
		return wmsAlarmManage.deleteWmsAlarmVOByID(id);
	}
}
