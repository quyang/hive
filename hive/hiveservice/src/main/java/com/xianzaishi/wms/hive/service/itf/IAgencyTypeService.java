package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.AgencyTypeVO;
import com.xianzaishi.wms.hive.vo.AgencyTypeQueryVO;

public interface IAgencyTypeService {

	public Boolean addAgencyTypeVO(AgencyTypeVO agencyTypeVO);

	public List<AgencyTypeVO> queryAgencyTypeVOList(AgencyTypeQueryVO agencyTypeQueryVO);

	public AgencyTypeVO getAgencyTypeVOByID(Long id);

	public Boolean modifyAgencyTypeVO(AgencyTypeVO agencyTypeVO);

	public Boolean deleteAgencyTypeVOByID(Long id);

}