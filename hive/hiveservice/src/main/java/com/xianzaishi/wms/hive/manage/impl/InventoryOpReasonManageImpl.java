package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IInventoryOpReasonDao;
import com.xianzaishi.wms.hive.manage.itf.IInventoryOpReasonManage;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;

public class InventoryOpReasonManageImpl implements IInventoryOpReasonManage {
	@Autowired
	private IInventoryOpReasonDao inventoryOpReasonDao = null;

	private void validate(InventoryOpReasonVO inventoryOpReasonVO) {
		if (inventoryOpReasonVO.getOpType() == null
				|| inventoryOpReasonVO.getOpType() <= 0) {
			throw new BizException("opType error："
					+ inventoryOpReasonVO.getOpType());
		}
		if (inventoryOpReasonVO.getReasonName() == null
				|| inventoryOpReasonVO.getReasonName().isEmpty()) {
			throw new BizException("reasonName error："
					+ inventoryOpReasonVO.getReasonName());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInventoryOpReasonDao getInventoryOpReasonDao() {
		return inventoryOpReasonDao;
	}

	public void setInventoryOpReasonDao(
			IInventoryOpReasonDao inventoryOpReasonDao) {
		this.inventoryOpReasonDao = inventoryOpReasonDao;
	}

	public Boolean addInventoryOpReasonVO(
			InventoryOpReasonVO inventoryOpReasonVO) {
		inventoryOpReasonDao.addDO(inventoryOpReasonVO);
		return true;
	}

	public List<InventoryOpReasonVO> queryInventoryOpReasonVOList(
			InventoryOpReasonQueryVO inventoryOpReasonQueryVO) {
		return inventoryOpReasonDao.queryDO(inventoryOpReasonQueryVO);
	}

	public InventoryOpReasonVO getInventoryOpReasonVOByID(Long id) {
		return (InventoryOpReasonVO) inventoryOpReasonDao.getDOByID(id);
	}

	public Boolean modifyInventoryOpReasonVO(
			InventoryOpReasonVO inventoryOpReasonVO) {
		return inventoryOpReasonDao.updateDO(inventoryOpReasonVO);
	}

	public Boolean deleteInventoryOpReasonVOByID(Long id) {
		return inventoryOpReasonDao.delDO(id);
	}
}
