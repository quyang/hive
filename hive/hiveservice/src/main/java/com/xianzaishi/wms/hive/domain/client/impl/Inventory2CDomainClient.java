package com.xianzaishi.wms.hive.domain.client.impl;

import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryDomainService;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public class Inventory2CDomainClient implements IInventory2CDomainClient {
	private static final Logger logger = Logger
			.getLogger(Inventory2CDomainClient.class);
	@Autowired
	private IInventoryDomainService inventoryDomainService = null;

	public IInventoryDomainService getInventoryDomainService() {
		return inventoryDomainService;
	}

	public void setInventoryDomainService(
			IInventoryDomainService inventoryDomainService) {
		this.inventoryDomainService = inventoryDomainService;
	}

	public SimpleResultVO<InventoryVO> getInventoryBySKUID(Long agencyID,
			Long skuID) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.get2CInventoryBySKUID(agencyID, skuID));
	}

	public SimpleResultVO<InventoryVO> getInventoryByGovAndSKU(Long govID,
			Long skuID) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.get2CInventoryBySKUID(govID, skuID));
	}

	@Override
	public SimpleResultVO setGuardInventoryBySkuId(Long skuId, String guradCount) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.setGuardInventoryBySkuId(skuId, guradCount));

	}

	/**
	 * 插入安全库存信息
	 * @param data
	 * @return
	 */
	@Override
	public SimpleResultVO insettGuardInventoryInfo(WmsGuardBitQueryVO data) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.insettGuardInventoryInfo(data));
	}

	public SimpleResultVO<List<InventoryVO>> batchGetInventoryBySKUID(
			Long agencyID, List<Long> skuID) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.batchGet2CInventoryBySKUID(agencyID, skuID));
	}

	public SimpleResultVO<List<InventoryVO>> batchGetInventoryByGovAndSKU(
			Long govID, List<Long> skuID) {
		return SimpleResultVO.buildSuccessResult(inventoryDomainService
				.batchGet2CInventoryBySKUID(govID, skuID));
	}
}
