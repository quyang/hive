package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.ISlottingTypeDao;

public class SlottingTypeDaoImpl extends BaseDaoAdapter implements
		ISlottingTypeDao {
	public String getVOClassName() {
		return "SlottingTypeDO";
	}
}
