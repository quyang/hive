package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;

public interface IPositionDetailService {
	public Long addPositionDetailVO(PositionDetailVO positionDetailVO);

	public List<PositionDetailVO> queryPositionDetailVOList(
			PositionDetailQueryVO positionDetailQueryVO);

	public PositionDetailVO getPositionDetailVOByID(Long id);

	public Boolean modifyPositionDetailVO(PositionDetailVO positionDetailVO);

	public Boolean deletePositionDetailVOByID(Long id);

}