package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsConfigTypeVO;
import com.xianzaishi.wms.hive.vo.WmsConfigTypeDO;
import com.xianzaishi.wms.hive.vo.WmsConfigTypeQueryVO;

public interface IWmsConfigTypeManage {

	public Boolean addWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO);

	public List<WmsConfigTypeVO> queryWmsConfigTypeVOList(WmsConfigTypeQueryVO wmsConfigTypeQueryVO);

	public WmsConfigTypeVO getWmsConfigTypeVOByID(Long id);

	public Boolean modifyWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO);

	public Boolean deleteWmsConfigTypeVOByID(Long id);

}