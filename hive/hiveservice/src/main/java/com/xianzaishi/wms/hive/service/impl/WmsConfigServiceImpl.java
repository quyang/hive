package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsConfigVO;
import com.xianzaishi.wms.hive.vo.WmsConfigQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsConfigService;
import com.xianzaishi.wms.hive.manage.itf.IWmsConfigManage;

public class WmsConfigServiceImpl implements IWmsConfigService {
	@Autowired
	private IWmsConfigManage wmsConfigManage = null;

	public IWmsConfigManage getWmsConfigManage() {
		return wmsConfigManage;
	}

	public void setWmsConfigManage(IWmsConfigManage wmsConfigManage) {
		this.wmsConfigManage = wmsConfigManage;
	}
	
	public Boolean addWmsConfigVO(WmsConfigVO wmsConfigVO) {
		wmsConfigManage.addWmsConfigVO(wmsConfigVO);
		return true;
	}

	public List<WmsConfigVO> queryWmsConfigVOList(WmsConfigQueryVO wmsConfigQueryVO) {
		return wmsConfigManage.queryWmsConfigVOList(wmsConfigQueryVO);
	}

	public WmsConfigVO getWmsConfigVOByID(Long id) {
		return (WmsConfigVO) wmsConfigManage.getWmsConfigVOByID(id);
	}

	public Boolean modifyWmsConfigVO(WmsConfigVO wmsConfigVO) {
		return wmsConfigManage.modifyWmsConfigVO(wmsConfigVO);
	}

	public Boolean deleteWmsConfigVOByID(Long id) {
		return wmsConfigManage.deleteWmsConfigVOByID(id);
	}
}
