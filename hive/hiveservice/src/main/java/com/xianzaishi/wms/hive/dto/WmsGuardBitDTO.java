package com.xianzaishi.wms.hive.dto;

import com.xianzaishi.wms.hive.vo.WmsGuardBitDO;

public class WmsGuardBitDTO extends WmsGuardBitDO {


  /**
   * 安全库存类型
   */
  public static class GuardTypeContants{

    /**
     * 表示按数量设置安全库存
     */
    public static Integer COUNT = 0 ;

    /**
     * 表示按比例设置安全仓库
     */
    public static Integer SCALE  = 1 ;

  }

}