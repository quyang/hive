package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonDO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonQueryVO;

public interface IInventoryOpReasonManage {

	public Boolean addInventoryOpReasonVO(InventoryOpReasonVO inventoryOpReasonVO);

	public List<InventoryOpReasonVO> queryInventoryOpReasonVOList(InventoryOpReasonQueryVO inventoryOpReasonQueryVO);

	public InventoryOpReasonVO getInventoryOpReasonVOByID(Long id);

	public Boolean modifyInventoryOpReasonVO(InventoryOpReasonVO inventoryOpReasonVO);

	public Boolean deleteInventoryOpReasonVOByID(Long id);

}