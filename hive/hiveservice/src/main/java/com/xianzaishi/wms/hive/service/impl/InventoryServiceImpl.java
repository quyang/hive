package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.manage.itf.IInventoryManage;
import com.xianzaishi.wms.hive.service.itf.IInventoryService;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public class InventoryServiceImpl implements IInventoryService {
	@Autowired
	private IInventoryManage inventoryManage = null;

	public IInventoryManage getInventoryManage() {
		return inventoryManage;
	}

	public void setInventoryManage(IInventoryManage inventoryManage) {
		this.inventoryManage = inventoryManage;
	}

	public Boolean addInventoryVO(InventoryVO inventoryVO) {
		inventoryManage.addInventoryVO(inventoryVO);
		return true;
	}

	public List<InventoryVO> queryInventoryVOList(
			InventoryQueryVO inventoryQueryVO) {
		return inventoryManage.queryInventoryVOList(inventoryQueryVO);
	}

	public InventoryVO getInventoryVOByID(Long id) {
		return (InventoryVO) inventoryManage.getInventoryVOByID(id);
	}

	public Boolean modifyInventoryVO(InventoryVO inventoryVO) {
		return inventoryManage.modifyInventoryVO(inventoryVO);
	}

	public Boolean deleteInventoryVOByID(Long id) {
		return inventoryManage.deleteInventoryVOByID(id);
	}

	public Integer get2CInventory(Long agencyID, Long skuID) {
		return inventoryManage.get2CInventory(agencyID, skuID);
	}

	public List<InventoryVO> batchGet2CInventory(Long agencyID,
			List<Long> skuIDs) {
		return inventoryManage.batchGet2CInventory(agencyID, skuIDs);
	}
}
