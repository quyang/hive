package com.xianzaishi.wms.hive.domain.service.impl;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryOpReasonDomainService;
import com.xianzaishi.wms.hive.service.itf.IInventoryOpReasonService;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryOpReasonDomainServiceImpl implements
		IInventoryOpReasonDomainService {

	private static final Logger logger = Logger.getLogger(InventoryOpReasonDomainServiceImpl.class);

	@Autowired
	private IInventoryOpReasonService inventoryOpReasonService = null;

	public List<InventoryOpReasonVO> getInventoryOpReasonByOpType(Long opType) {
		InventoryOpReasonQueryVO queryVO = new InventoryOpReasonQueryVO();
		queryVO.setOpType(opType);
		logger.error("InventoryOpReasonQueryVO数据=" + JackSonUtil.getJson(queryVO));
		return inventoryOpReasonService.queryInventoryOpReasonVOList(queryVO);
	}

	public IInventoryOpReasonService getInventoryOpReasonService() {
		return inventoryOpReasonService;
	}

	public void setInventoryOpReasonService(
			IInventoryOpReasonService inventoryOpReasonService) {
		this.inventoryOpReasonService = inventoryOpReasonService;
	}

}
