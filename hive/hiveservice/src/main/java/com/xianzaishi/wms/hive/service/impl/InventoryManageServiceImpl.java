package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.manage.itf.IInventoryManageManage;
import com.xianzaishi.wms.hive.service.itf.IInventoryManageService;
import com.xianzaishi.wms.hive.vo.InventoryManageQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public class InventoryManageServiceImpl implements IInventoryManageService {
	@Autowired
	private IInventoryManageManage inventoryManageManage = null;

	public IInventoryManageManage getInventoryManageManage() {
		return inventoryManageManage;
	}

	public void setInventoryManageManage(
			IInventoryManageManage inventoryManageManage) {
		this.inventoryManageManage = inventoryManageManage;
	}

	public Long addInventoryManageVO(InventoryManageVO inventoryManageVO) {
		return inventoryManageManage.addInventoryManageVO(inventoryManageVO);
	}

	public List<InventoryManageVO> queryInventoryManageVOList(
			InventoryManageQueryVO inventoryManageQueryVO) {
		return inventoryManageManage
				.queryInventoryManageVOList(inventoryManageQueryVO);
	}

	public InventoryManageVO getInventoryManageVOByID(Long id) {
		return (InventoryManageVO) inventoryManageManage
				.getInventoryManageVOByID(id);
	}

	public Boolean modifyInventoryManageVO(InventoryManageVO inventoryManageVO) {
		return inventoryManageManage.modifyInventoryManageVO(inventoryManageVO);
	}

	public Boolean deleteInventoryManageVOByID(Long id) {
		return inventoryManageManage.deleteInventoryManageVOByID(id);
	}
}
