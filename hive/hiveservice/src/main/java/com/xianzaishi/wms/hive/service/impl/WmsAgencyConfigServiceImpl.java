package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsAgencyConfigVO;
import com.xianzaishi.wms.hive.vo.WmsAgencyConfigQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsAgencyConfigService;
import com.xianzaishi.wms.hive.manage.itf.IWmsAgencyConfigManage;

public class WmsAgencyConfigServiceImpl implements IWmsAgencyConfigService {
	@Autowired
	private IWmsAgencyConfigManage wmsAgencyConfigManage = null;

	public IWmsAgencyConfigManage getWmsAgencyConfigManage() {
		return wmsAgencyConfigManage;
	}

	public void setWmsAgencyConfigManage(IWmsAgencyConfigManage wmsAgencyConfigManage) {
		this.wmsAgencyConfigManage = wmsAgencyConfigManage;
	}
	
	public Boolean addWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO) {
		wmsAgencyConfigManage.addWmsAgencyConfigVO(wmsAgencyConfigVO);
		return true;
	}

	public List<WmsAgencyConfigVO> queryWmsAgencyConfigVOList(WmsAgencyConfigQueryVO wmsAgencyConfigQueryVO) {
		return wmsAgencyConfigManage.queryWmsAgencyConfigVOList(wmsAgencyConfigQueryVO);
	}

	public WmsAgencyConfigVO getWmsAgencyConfigVOByID(Long id) {
		return (WmsAgencyConfigVO) wmsAgencyConfigManage.getWmsAgencyConfigVOByID(id);
	}

	public Boolean modifyWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO) {
		return wmsAgencyConfigManage.modifyWmsAgencyConfigVO(wmsAgencyConfigVO);
	}

	public Boolean deleteWmsAgencyConfigVOByID(Long id) {
		return wmsAgencyConfigManage.deleteWmsAgencyConfigVOByID(id);
	}
}
