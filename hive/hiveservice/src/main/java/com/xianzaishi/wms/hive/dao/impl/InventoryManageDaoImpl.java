package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IInventoryManageDao;

public class InventoryManageDaoImpl extends BaseDaoAdapter implements
		IInventoryManageDao {
	public String getVOClassName() {
		return "InventoryManageDO";
	}
}
