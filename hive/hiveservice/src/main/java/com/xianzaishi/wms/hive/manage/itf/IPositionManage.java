package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.hive.vo.PositionDO;
import com.xianzaishi.wms.hive.vo.PositionQueryVO;

public interface IPositionManage {

	public Boolean addPositionVO(PositionVO positionVO);

	public List<PositionVO> queryPositionVOList(PositionQueryVO positionQueryVO);

	public PositionVO getPositionVOByID(Long id);

	public Boolean modifyPositionVO(PositionVO positionVO);

	public Boolean deletePositionVOByID(Long id);

}