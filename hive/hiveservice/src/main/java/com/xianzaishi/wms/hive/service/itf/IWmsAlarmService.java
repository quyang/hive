package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsAlarmVO;
import com.xianzaishi.wms.hive.vo.WmsAlarmQueryVO;

public interface IWmsAlarmService {

	public Boolean addWmsAlarmVO(WmsAlarmVO wmsAlarmVO);

	public List<WmsAlarmVO> queryWmsAlarmVOList(WmsAlarmQueryVO wmsAlarmQueryVO);

	public WmsAlarmVO getWmsAlarmVOByID(Long id);

	public Boolean modifyWmsAlarmVO(WmsAlarmVO wmsAlarmVO);

	public Boolean deleteWmsAlarmVOByID(Long id);

}