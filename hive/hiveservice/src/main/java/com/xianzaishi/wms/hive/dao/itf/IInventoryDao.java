package com.xianzaishi.wms.hive.dao.itf;

import java.util.List;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public interface IInventoryDao extends IBaseDao {
	public Integer get2CInventory(Long agencyID, Long skuID);

	public List<InventoryVO> batchGet2CInventory(Long agencyID,
			List<Long> skuIDs);
}