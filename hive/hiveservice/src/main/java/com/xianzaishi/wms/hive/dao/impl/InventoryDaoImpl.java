package com.xianzaishi.wms.hive.dao.impl;

import java.util.List;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IInventoryDao;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public class InventoryDaoImpl extends BaseDaoAdapter implements IInventoryDao {
	public String getVOClassName() {
		return "InventoryDO";
	}

	public Integer get2CInventory(Long agencyID, Long skuID) {
		Integer flag = null;
		try {
			InventoryQueryVO queryVO = new InventoryQueryVO();
			queryVO.setAgencyId(agencyID);
			queryVO.setSkuId(skuID);
			flag = (Integer) simpleSqlMapClientTemplate.queryForObject(
					getVOClassName() + ".get2CInventory", queryVO);
		} catch (Exception e) {
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}

	public List<InventoryVO> batchGet2CInventory(Long agencyID,
			List<Long> skuIDs) {
		List<InventoryVO> flag = null;
		try {
			InventoryQueryVO queryVO = new InventoryQueryVO();
			queryVO.setAgencyId(agencyID);
			queryVO.setSkuIds(skuIDs);
			flag = (List<InventoryVO>) simpleSqlMapClientTemplate.queryForList(
					getVOClassName() + ".batchGet2CInventory", queryVO);
		} catch (Exception e) {
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}
}
