package com.xianzaishi.wms.hive.domain.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.domain.client.itf.IBarcodeSequenceDomainClient;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.domain.service.itf.IPositionDomainService;
import com.xianzaishi.wms.hive.service.itf.IPositionService;
import com.xianzaishi.wms.hive.vo.PositionQueryVO;
import com.xianzaishi.wms.hive.vo.PositionVO;

public class PositionDomainServiceImpl implements IPositionDomainService {

	@Autowired
	private IBarcodeSequenceDomainClient barcodeSequenceDomainClient = null;
	@Autowired
	private IPositionService positionService = null;

	public PositionVO getPositonByBarcode(String barcode) {
		PositionQueryVO positionQueryVO = new PositionQueryVO();
		positionQueryVO.setBarcode(barcode);
		List<PositionVO> positions = positionService
				.queryPositionVOList(positionQueryVO);
		if (positions == null || positions.size() != 1) {
			throw new BizException("获取仓库位错误:{barcode:" + barcode + "}");
		}
		return positions.get(0);
	}

	public IPositionService getPositionService() {
		return positionService;
	}

	public void setPositionService(IPositionService positionService) {
		this.positionService = positionService;
	}

	public void addPosition(PositionVO positionVO) {
		positionVO.setBarcode(barcodeSequenceDomainClient.getBarcode(1)
				.getTarget());
		positionService.addPositionVO(positionVO);
	}

	public IBarcodeSequenceDomainClient getBarcodeSequenceDomainClient() {
		return barcodeSequenceDomainClient;
	}

	public void setBarcodeSequenceDomainClient(
			IBarcodeSequenceDomainClient barcodeSequenceDomainClient) {
		this.barcodeSequenceDomainClient = barcodeSequenceDomainClient;
	}

	public PositionVO getPositionVOByID(Long id) {
		return positionService.getPositionVOByID(id);
	}

	public List<PositionVO> batchGetPositionVOByBarcode(List<String> barcodes)
			throws Exception {
		if (barcodes == null && barcodes.isEmpty()) {
			throw new Exception("barcodes is empty");
		}
		List<PositionVO> positionVOs = new LinkedList<PositionVO>();
		for (String barcode : barcodes) {
			positionVOs.add(getPositonByBarcode(barcode));
		}
		return positionVOs;
	}

	public List<PositionVO> batchGetPositionVOByID(List<Long> ids)
			throws Exception {
		if (ids == null && ids.isEmpty()) {
			throw new Exception("ids is empty");
		}
		List<PositionVO> positionVOs = new LinkedList<PositionVO>();
		for (Long id : ids) {
			positionVOs.add(getPositionVOByID(id));
		}
		return positionVOs;
	}

	public PositionVO getPositonByCode(Long agencyID, String code) {
		PositionQueryVO positionQueryVO = new PositionQueryVO();
		positionQueryVO.setCode(code);
		positionQueryVO.setAgencyId(agencyID);
		List<PositionVO> positions = positionService
				.queryPositionVOList(positionQueryVO);
		if (positions == null || positions.size() != 1) {
			throw new BizException("获取仓库位错误:{code:" + code + "}");
		}
		return positions.get(0);
	}
}
