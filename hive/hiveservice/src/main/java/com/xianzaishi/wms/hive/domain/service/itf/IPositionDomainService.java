package com.xianzaishi.wms.hive.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.PositionVO;

public interface IPositionDomainService {
	public PositionVO getPositonByBarcode(String barcode);

	public void addPosition(PositionVO positionVO);

	public PositionVO getPositionVOByID(Long id);

	public List<PositionVO> batchGetPositionVOByBarcode(List<String> barcodes)
			throws Exception;

	public List<PositionVO> batchGetPositionVOByID(List<Long> ids)
			throws Exception;

	public PositionVO getPositonByCode(Long agencyID, String code);
}
