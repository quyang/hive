package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.StoryAreaVO;
import com.xianzaishi.wms.hive.vo.StoryAreaQueryVO;

public interface IStoryAreaService {

	public Boolean addStoryAreaVO(StoryAreaVO storyAreaVO);

	public List<StoryAreaVO> queryStoryAreaVOList(StoryAreaQueryVO storyAreaQueryVO);

	public StoryAreaVO getStoryAreaVOByID(Long id);

	public Boolean modifyStoryAreaVO(StoryAreaVO storyAreaVO);

	public Boolean deleteStoryAreaVOByID(Long id);

}