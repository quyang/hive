package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IAgencyDao;
import com.xianzaishi.wms.hive.manage.itf.IAgencyManage;
import com.xianzaishi.wms.hive.vo.AgencyQueryVO;
import com.xianzaishi.wms.hive.vo.AgencyVO;

public class AgencyManageImpl implements IAgencyManage {
	@Autowired
	private IAgencyDao agencyDao = null;

	private void validate(AgencyVO agencyVO) {
		if (agencyVO.getName() == null || agencyVO.getName().isEmpty()) {
			throw new BizException("name error：" + agencyVO.getName());
		}
		if (agencyVO.getType() == null || agencyVO.getType() <= 0) {
			throw new BizException("type error：" + agencyVO.getType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IAgencyDao getAgencyDao() {
		return agencyDao;
	}

	public void setAgencyDao(IAgencyDao agencyDao) {
		this.agencyDao = agencyDao;
	}

	public Boolean addAgencyVO(AgencyVO agencyVO) {
		validate(agencyVO);
		agencyDao.addDO(agencyVO);
		return true;
	}

	public List<AgencyVO> queryAgencyVOList(AgencyQueryVO agencyQueryVO) {
		return agencyDao.queryDO(agencyQueryVO);
	}

	public AgencyVO getAgencyVOByID(Long id) {
		return (AgencyVO) agencyDao.getDOByID(id);
	}

	public Boolean modifyAgencyVO(AgencyVO agencyVO) {
		return agencyDao.updateDO(agencyVO);
	}

	public Boolean deleteAgencyVOByID(Long id) {
		return agencyDao.delDO(id);
	}
}
