package com.xianzaishi.wms.hive.domain.service.itf;

import com.xianzaishi.wms.hive.vo.AgencyVO;

public interface IAgencyDomainService {
	public AgencyVO getAgencyByID(Long agencyID);
}
