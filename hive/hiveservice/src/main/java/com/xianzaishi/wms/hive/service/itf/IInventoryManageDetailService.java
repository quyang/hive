package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryManageDetailQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;

public interface IInventoryManageDetailService {

	public Boolean addInventoryManageDetailVO(
			InventoryManageDetailVO inventoryManageDetailVO);

	public List<InventoryManageDetailVO> queryInventoryManageDetailVOList(
			InventoryManageDetailQueryVO inventoryManageDetailQueryVO);

	public InventoryManageDetailVO getInventoryManageDetailVOByID(Long id);

	public Boolean modifyInventoryManageDetailVO(
			InventoryManageDetailVO inventoryManageDetailVO);

	public Boolean deleteInventoryManageDetailVOByID(Long id);

	public List<InventoryManageDetailVO> getInventoryManageDetailVOByInventoryManageID(
			Long id);

	public Boolean batchAddInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs);

	public Boolean batchModifyInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs);

	public Boolean batchDeleteInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs);

	public Boolean batchDeleteInventoryManageDetailVOByID(
			List<Long> storyDetailIDs);
}