package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IPositionDetailService;
import com.xianzaishi.wms.hive.manage.itf.IPositionDetailManage;

public class PositionDetailServiceImpl implements IPositionDetailService {
	@Autowired
	private IPositionDetailManage positionDetailManage = null;

	public IPositionDetailManage getPositionDetailManage() {
		return positionDetailManage;
	}

	public void setPositionDetailManage(
			IPositionDetailManage positionDetailManage) {
		this.positionDetailManage = positionDetailManage;
	}

	public Long addPositionDetailVO(PositionDetailVO positionDetailVO) {
		return positionDetailManage.addPositionDetailVO(positionDetailVO);
	}

	public List<PositionDetailVO> queryPositionDetailVOList(
			PositionDetailQueryVO positionDetailQueryVO) {
		return positionDetailManage
				.queryPositionDetailVOList(positionDetailQueryVO);
	}

	public PositionDetailVO getPositionDetailVOByID(Long id) {
		return (PositionDetailVO) positionDetailManage
				.getPositionDetailVOByID(id);
	}

	public Boolean modifyPositionDetailVO(PositionDetailVO positionDetailVO) {
		return positionDetailManage.modifyPositionDetailVO(positionDetailVO);
	}

	public Boolean deletePositionDetailVOByID(Long id) {
		return positionDetailManage.deletePositionDetailVOByID(id);
	}

}
