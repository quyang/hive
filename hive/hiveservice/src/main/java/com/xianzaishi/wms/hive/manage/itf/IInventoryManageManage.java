package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryManageQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public interface IInventoryManageManage {

	public Long addInventoryManageVO(InventoryManageVO inventoryManageVO);

	public List<InventoryManageVO> queryInventoryManageVOList(
			InventoryManageQueryVO inventoryManageQueryVO);

	public InventoryManageVO getInventoryManageVOByID(Long id);

	public Boolean modifyInventoryManageVO(InventoryManageVO inventoryManageVO);

	public Boolean deleteInventoryManageVOByID(Long id);

}