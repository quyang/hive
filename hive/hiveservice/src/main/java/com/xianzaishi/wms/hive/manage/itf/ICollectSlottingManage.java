package com.xianzaishi.wms.hive.manage.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.CollectSlottingVO;
import com.xianzaishi.wms.hive.vo.CollectSlottingDO;
import com.xianzaishi.wms.hive.vo.CollectSlottingQueryVO;

public interface ICollectSlottingManage {

	public Boolean addCollectSlottingVO(CollectSlottingVO collectSlottingVO);

	public List<CollectSlottingVO> queryCollectSlottingVOList(CollectSlottingQueryVO collectSlottingQueryVO);

	public CollectSlottingVO getCollectSlottingVOByID(Long id);

	public Boolean modifyCollectSlottingVO(CollectSlottingVO collectSlottingVO);

	public Boolean deleteCollectSlottingVOByID(Long id);

}