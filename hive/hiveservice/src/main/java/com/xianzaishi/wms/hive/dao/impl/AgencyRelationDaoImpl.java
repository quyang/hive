package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.hive.dao.itf.IAgencyRelationDao;
import com.xianzaishi.wms.hive.vo.AgencyRelationVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class AgencyRelationDaoImpl extends BaseDaoAdapter implements IAgencyRelationDao {
public String getVOClassName() {
		return "AgencyRelationDO";
	}
}
