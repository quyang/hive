package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.manage.itf.IInventoryManageDetailManage;
import com.xianzaishi.wms.hive.service.itf.IInventoryManageDetailService;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;

public class InventoryManageDetailServiceImpl implements IInventoryManageDetailService {
	@Autowired
	private IInventoryManageDetailManage inventoryManageDetailManage = null;

	public IInventoryManageDetailManage getInventoryManageDetailManage() {
		return inventoryManageDetailManage;
	}

	public void setInventoryManageDetailManage(IInventoryManageDetailManage inventoryManageDetailManage) {
		this.inventoryManageDetailManage = inventoryManageDetailManage;
	}
	
	public Boolean addInventoryManageDetailVO(InventoryManageDetailVO inventoryManageDetailVO) {
		inventoryManageDetailManage.addInventoryManageDetailVO(inventoryManageDetailVO);
		return true;
	}

	public List<InventoryManageDetailVO> queryInventoryManageDetailVOList(InventoryManageDetailQueryVO inventoryManageDetailQueryVO) {
		return inventoryManageDetailManage.queryInventoryManageDetailVOList(inventoryManageDetailQueryVO);
	}

	public InventoryManageDetailVO getInventoryManageDetailVOByID(Long id) {
		return (InventoryManageDetailVO) inventoryManageDetailManage.getInventoryManageDetailVOByID(id);
	}

	public Boolean modifyInventoryManageDetailVO(InventoryManageDetailVO inventoryManageDetailVO) {
		return inventoryManageDetailManage.modifyInventoryManageDetailVO(inventoryManageDetailVO);
	}

	public Boolean deleteInventoryManageDetailVOByID(Long id) {
		return inventoryManageDetailManage.deleteInventoryManageDetailVOByID(id);
	}	public List<InventoryManageDetailVO> getInventoryManageDetailVOByInventoryManageID(Long id) {
		return inventoryManageDetailManage.getInventoryManageDetailVOByInventoryManageID(id);
	}

	public Boolean batchAddInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailManage.batchAddInventoryManageDetailVO(inventoryManageDetailVOs);
	}

	public Boolean batchModifyInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailManage
				.batchModifyInventoryManageDetailVO(inventoryManageDetailVOs);
	}

	public Boolean batchDeleteInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailManage
				.batchDeleteInventoryManageDetailVO(inventoryManageDetailVOs);
	}

	public Boolean batchDeleteInventoryManageDetailVOByID(List<Long> storyDetailIDs) {
		return inventoryManageDetailManage
				.batchDeleteInventoryManageDetailVOByID(storyDetailIDs);
	}
}
