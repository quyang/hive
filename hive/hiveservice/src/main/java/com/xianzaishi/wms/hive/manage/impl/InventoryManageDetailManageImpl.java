package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IInventoryManageDetailDao;
import com.xianzaishi.wms.hive.manage.itf.IInventoryManageDetailManage;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;

public class InventoryManageDetailManageImpl implements
		IInventoryManageDetailManage {
	@Autowired
	private IInventoryManageDetailDao inventoryManageDetailDao = null;

	private void validate(InventoryManageDetailVO inventoryManageDetailVO) {
		if (inventoryManageDetailVO.getManageId() == null
				|| inventoryManageDetailVO.getManageId() <= 0) {
			throw new BizException("manageID error："
					+ inventoryManageDetailVO.getManageId());
		}
		if (inventoryManageDetailVO.getNumber() == null) {
			throw new BizException("number error："
					+ inventoryManageDetailVO.getNumber());
		}
		if (inventoryManageDetailVO.getPositionId() == null
				|| inventoryManageDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ inventoryManageDetailVO.getPositionId());
		}
		if (inventoryManageDetailVO.getSkuId() == null
				|| inventoryManageDetailVO.getSkuId() <= 0) {
			throw new BizException("SkuID error："
					+ inventoryManageDetailVO.getSkuId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInventoryManageDetailDao getInventoryManageDetailDao() {
		return inventoryManageDetailDao;
	}

	public void setInventoryManageDetailDao(
			IInventoryManageDetailDao inventoryManageDetailDao) {
		this.inventoryManageDetailDao = inventoryManageDetailDao;
	}

	public Boolean addInventoryManageDetailVO(
			InventoryManageDetailVO inventoryManageDetailVO) {
		validate(inventoryManageDetailVO);
		inventoryManageDetailDao.addDO(inventoryManageDetailVO);
		return true;
	}

	public List<InventoryManageDetailVO> queryInventoryManageDetailVOList(
			InventoryManageDetailQueryVO inventoryManageDetailQueryVO) {
		return inventoryManageDetailDao.queryDO(inventoryManageDetailQueryVO);
	}

	public InventoryManageDetailVO getInventoryManageDetailVOByID(Long id) {
		return (InventoryManageDetailVO) inventoryManageDetailDao.getDOByID(id);
	}

	public Boolean modifyInventoryManageDetailVO(
			InventoryManageDetailVO inventoryManageDetailVO) {
		return inventoryManageDetailDao.updateDO(inventoryManageDetailVO);
	}

	public Boolean deleteInventoryManageDetailVOByID(Long id) {
		return inventoryManageDetailDao.delDO(id);
	}

	public List<InventoryManageDetailVO> getInventoryManageDetailVOByInventoryManageID(
			Long id) {
		InventoryManageDetailQueryVO queryVO = new InventoryManageDetailQueryVO();
		queryVO.setManageId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return inventoryManageDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailDao.batchAddDO(inventoryManageDetailVOs);
	}

	public Boolean batchModifyInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailDao.batchModifyDO(inventoryManageDetailVOs);
	}

	public Boolean batchDeleteInventoryManageDetailVO(
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		return inventoryManageDetailDao.batchDeleteDO(inventoryManageDetailVOs);
	}

	public Boolean batchDeleteInventoryManageDetailVOByID(
			List<Long> inventoryManageDetailIDs) {
		return inventoryManageDetailDao
				.batchDeleteDOByID(inventoryManageDetailIDs);
	}
}
