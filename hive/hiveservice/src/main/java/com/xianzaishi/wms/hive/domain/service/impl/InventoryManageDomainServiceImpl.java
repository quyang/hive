package com.xianzaishi.wms.hive.domain.service.impl;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.ListUtils;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryDomainService;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryManageDomainService;
import com.xianzaishi.wms.hive.service.itf.IInventoryManageDetailService;
import com.xianzaishi.wms.hive.service.itf.IInventoryManageService;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryManageDomainServiceImpl implements
		IInventoryManageDomainService {
	@Autowired
	private IInventoryManageService inventoryManageService = null;
	@Autowired
	private IInventoryManageDetailService inventoryManageDetailService = null;
	@Autowired
	private IInventoryDomainService inventoryDomainService = null;
	@Autowired
	private PurchaseService purchaseService;
	@Autowired
	private SkuService skuService;

	private static final Logger logger = Logger.getLogger(InventoryManageDomainServiceImpl.class);

	public Boolean createInventoryManageDomain(
			InventoryManageVO inventoryManageVO) {
		if (inventoryManageVO.getDetails() == null
				|| inventoryManageVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}

		Long inventoryManageID = inventoryManageService
				.addInventoryManageVO(inventoryManageVO);

		initDetails(inventoryManageID, inventoryManageVO.getDetails());

		inventoryManageDetailService
				.batchAddInventoryManageDetailVO(inventoryManageVO.getDetails());

		syncInventory(inventoryManageVO);
		return true;
	}

	public InventoryManageVO getInventoryManageDomainByID(Long inventoryManageID) {
		if (inventoryManageID == null || inventoryManageID <= 0) {
			throw new BizException("id error");
		}

		InventoryManageVO inventoryManageVO = inventoryManageService
				.getInventoryManageVOByID(inventoryManageID);

		inventoryManageVO
				.setDetails(inventoryManageDetailService
						.getInventoryManageDetailVOByInventoryManageID(inventoryManageID));

		return inventoryManageVO;
	}

	public void syncInventory(InventoryManageVO inventoryManageVO) {
		for (int i = 0; i < inventoryManageVO.getDetails().size(); i++) {
			inventoryDomainService.syncInventory(inventoryManageVO
					.getAgencyId(), (InventoryManageDetailVO) inventoryManageVO
					.getDetails().get(i));
		}
	}

	@Override
	public Boolean updateInventoryManageDomain(InventoryManageVO inventoryManageVO) {
		if (null == inventoryManageVO) {
			throw new BizException("数据对象为null");
		}

		//更新总流水
		notTrue(inventoryManageService.modifyInventoryManageVO(inventoryManageVO), "更新总流水信息失败");

		Result<List<PurchaseSubOrderDTO>> subPurchaseList = purchaseService.querySubPurchaseListByPurId(
				Integer.parseInt(String.valueOf(inventoryManageVO.getRelationId())));
		notSuccess(subPurchaseList,"查询子采购单信息不存在");
		ListUtils.isEmpty(subPurchaseList.getModule(),"子采购单信息为空");

		//更新子入库流水
		List<InventoryManageDetailVO> details = inventoryManageVO.getDetails();
		updatDetails(details,subPurchaseList.getModule());
		notTrue(inventoryManageDetailService.batchModifyInventoryManageDetailVO(details), "更新子入库流水失败");

		return true;
	}

	/**
	 * 更新子流水信息
	 * @param details 子入库流水集合
	 * @param subPurchaseList 子采购单集合
	 */
	private void updatDetails(List<InventoryManageDetailVO> details,
			List<PurchaseSubOrderDTO> subPurchaseList) {
		ListUtils.isEmpty(details,"子入库流水信息集合为空");
		ListUtils.isEmpty(subPurchaseList,"子采购单信息集合为空");

		for (InventoryManageDetailVO vo:details) {
			if (null == vo) {
				continue;
			}
			//获取前台skuid对应的子采购单信息
			PurchaseSubOrderDTO purchaseSubOrderDTO = getPurchaseSubOrderDTOByPurchaseIdAndSkuId(
					vo.getSkuId(), subPurchaseList);
			vo.setRelationId(Long.parseLong(String.valueOf(purchaseSubOrderDTO.getPurchaseSubId())));
		}
	}

	/**
	 * 根据前台skuId获取对应的自采购单信息
	 * @param skuId 前台skuId
	 * @param subPurchaseList 子采购订单数据集合
	 * @return 返回前台skuId对应的子采购单对象
	 */
	private PurchaseSubOrderDTO getPurchaseSubOrderDTOByPurchaseIdAndSkuId(Long skuId,
			List<PurchaseSubOrderDTO> subPurchaseList) {
		ObjectUtils.isNull(skuId, "商品skuId不能为null");
		ListUtils.isEmpty(subPurchaseList, "子采购单集合不能为空");

		PurchaseSubOrderDTO result = null;

		//获取后台skuId
		Long pskuId = getPskuIdByCskuId(skuId);
		logger.error("InventoryManageDomainServiceImpl，pskuId=" + pskuId);

		for (PurchaseSubOrderDTO subOrderDTO : subPurchaseList) {
			if (null == subOrderDTO) {
				continue;
			}

			if (pskuId.equals(subOrderDTO.getSkuId())) {
				 result = subOrderDTO;
				 break;
			}
		}

		if (null == result) {
			throw new BizException("从自采购单集合中没有查到符合条件的信息");
		}
		return result;
	}

	/**
	 * 根据前台skuId获取后台skuId
	 * @param skuId 前台skuId
	 * @return
	 */
	private Long getPskuIdByCskuId(Long skuId) {
		ObjectUtils.isNull(skuId, "skuId不能为null");
		Result<List<ItemSkuRelationDTO>> skuRelationList = skuService
				.querySkuRelationList(SkuRelationQuery.querySkuRelationByRelationTypeAndCskuId(skuId));
		notSuccess(skuRelationList, "商品skuId：" + skuId + "的没有查到对象的关系信息");
		ListUtils.isEmpty(skuRelationList.getModule(), "sku信息查询结果为空");

		Long pskuId = skuRelationList.getModule().get(0).getPskuId();//后台skuId
		ObjectUtils.isNull(pskuId,"查询到的后台skuId值为null" );
		return pskuId;
	}

	private void initDetails(Long id,
			List<InventoryManageDetailVO> inventoryManageDetailVOs) {
		for (int i = 0; i < inventoryManageDetailVOs.size(); i++) {
			inventoryManageDetailVOs.get(i).setManageId(id);
		}
	}

	public IInventoryManageService getInventoryManageService() {
		return inventoryManageService;
	}

	public void setInventoryManageService(
			IInventoryManageService inventoryManageService) {
		this.inventoryManageService = inventoryManageService;
	}

	public IInventoryManageDetailService getInventoryManageDetailService() {
		return inventoryManageDetailService;
	}

	public void setInventoryManageDetailService(
			IInventoryManageDetailService inventoryManageDetailService) {
		this.inventoryManageDetailService = inventoryManageDetailService;
	}

	public IInventoryDomainService getInventoryDomainService() {
		return inventoryDomainService;
	}

	public void setInventoryDomainService(
			IInventoryDomainService inventoryDomainService) {
		this.inventoryDomainService = inventoryDomainService;
	}

	public void notTrue(Boolean a, String showMsg) {
		if (null == a ) {
			throw new BizException("布尔参数为null");
		}
		if (!a) {
			throw new BizException(showMsg);
		}
	}

	public void notSuccess(Result result,String showMsg) {
		if (null == result) {
			throw new BizException("结果对象为空");
		}

		if (!result.getSuccess() || null == result.getModule()) {
			throw new BizException(showMsg);
		}

	}

}
