package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsDailySettlementLogDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsDailySettlementLogManage;
import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogQueryVO;
import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogVO;

public class WmsDailySettlementLogManageImpl implements
		IWmsDailySettlementLogManage {
	@Autowired
	private IWmsDailySettlementLogDao wmsDailySettlementLogDao = null;

	private void validate(WmsDailySettlementLogVO wmsDailySettlementLogVO) {
		if (wmsDailySettlementLogVO.getAgencyId() == null
				|| wmsDailySettlementLogVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ wmsDailySettlementLogVO.getAgencyId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsDailySettlementLogDao getWmsDailySettlementLogDao() {
		return wmsDailySettlementLogDao;
	}

	public void setWmsDailySettlementLogDao(
			IWmsDailySettlementLogDao wmsDailySettlementLogDao) {
		this.wmsDailySettlementLogDao = wmsDailySettlementLogDao;
	}

	public Boolean addWmsDailySettlementLogVO(
			WmsDailySettlementLogVO wmsDailySettlementLogVO) {
		validate(wmsDailySettlementLogVO);
		wmsDailySettlementLogDao.addDO(wmsDailySettlementLogVO);
		return true;
	}

	public List<WmsDailySettlementLogVO> queryWmsDailySettlementLogVOList(
			WmsDailySettlementLogQueryVO wmsDailySettlementLogQueryVO) {
		return wmsDailySettlementLogDao.queryDO(wmsDailySettlementLogQueryVO);
	}

	public WmsDailySettlementLogVO getWmsDailySettlementLogVOByID(Long id) {
		return (WmsDailySettlementLogVO) wmsDailySettlementLogDao.getDOByID(id);
	}

	public Boolean modifyWmsDailySettlementLogVO(
			WmsDailySettlementLogVO wmsDailySettlementLogVO) {
		return wmsDailySettlementLogDao.updateDO(wmsDailySettlementLogVO);
	}

	public Boolean deleteWmsDailySettlementLogVOByID(Long id) {
		return wmsDailySettlementLogDao.delDO(id);
	}
}
