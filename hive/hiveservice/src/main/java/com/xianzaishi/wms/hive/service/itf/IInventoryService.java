package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public interface IInventoryService {

	public Boolean addInventoryVO(InventoryVO inventoryVO);

	public List<InventoryVO> queryInventoryVOList(
			InventoryQueryVO inventoryQueryVO);

	public InventoryVO getInventoryVOByID(Long id);

	public Boolean modifyInventoryVO(InventoryVO inventoryVO);

	public Boolean deleteInventoryVOByID(Long id);

	public Integer get2CInventory(Long agencyID, Long skuID);

	public List<InventoryVO> batchGet2CInventory(Long agencyID,
			List<Long> skuIDs);

}