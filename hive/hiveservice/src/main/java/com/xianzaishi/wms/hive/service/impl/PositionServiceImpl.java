package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.manage.itf.IPositionManage;
import com.xianzaishi.wms.hive.service.itf.IPositionService;
import com.xianzaishi.wms.hive.vo.PositionQueryVO;
import com.xianzaishi.wms.hive.vo.PositionVO;

public class PositionServiceImpl implements IPositionService {
	@Autowired
	private IPositionManage positionManage = null;

	public IPositionManage getPositionManage() {
		return positionManage;
	}

	public void setPositionManage(IPositionManage positionManage) {
		this.positionManage = positionManage;
	}

	public Boolean addPositionVO(PositionVO positionVO) {

		positionManage.addPositionVO(positionVO);
		return true;
	}

	public List<PositionVO> queryPositionVOList(PositionQueryVO positionQueryVO) {
		return positionManage.queryPositionVOList(positionQueryVO);
	}

	public PositionVO getPositionVOByID(Long id) {
		return (PositionVO) positionManage.getPositionVOByID(id);
	}

	public Boolean modifyPositionVO(PositionVO positionVO) {
		return positionManage.modifyPositionVO(positionVO);
	}

	public Boolean deletePositionVOByID(Long id) {
		return positionManage.deletePositionVOByID(id);
	}
}
