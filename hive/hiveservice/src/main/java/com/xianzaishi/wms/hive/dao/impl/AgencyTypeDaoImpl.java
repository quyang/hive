package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.hive.dao.itf.IAgencyTypeDao;
import com.xianzaishi.wms.hive.vo.AgencyTypeVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class AgencyTypeDaoImpl extends BaseDaoAdapter implements IAgencyTypeDao {
public String getVOClassName() {
		return "AgencyTypeDO";
	}
}
