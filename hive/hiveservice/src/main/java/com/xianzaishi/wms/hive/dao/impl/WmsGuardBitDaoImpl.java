package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsGuardBitDao;

public class WmsGuardBitDaoImpl extends BaseDaoAdapter implements
		IWmsGuardBitDao {
	public String getVOClassName() {
		return "WmsGuardBitDO";
	}
}
