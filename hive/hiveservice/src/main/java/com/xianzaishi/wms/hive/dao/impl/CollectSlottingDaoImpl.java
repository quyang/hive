package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.ICollectSlottingDao;

public class CollectSlottingDaoImpl extends BaseDaoAdapter implements
		ICollectSlottingDao {
	public String getVOClassName() {
		return "CollectSlottingDO";
	}
}
