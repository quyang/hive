package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogVO;
import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsDailySettlementLogService;
import com.xianzaishi.wms.hive.manage.itf.IWmsDailySettlementLogManage;

public class WmsDailySettlementLogServiceImpl implements IWmsDailySettlementLogService {
	@Autowired
	private IWmsDailySettlementLogManage wmsDailySettlementLogManage = null;

	public IWmsDailySettlementLogManage getWmsDailySettlementLogManage() {
		return wmsDailySettlementLogManage;
	}

	public void setWmsDailySettlementLogManage(IWmsDailySettlementLogManage wmsDailySettlementLogManage) {
		this.wmsDailySettlementLogManage = wmsDailySettlementLogManage;
	}
	
	public Boolean addWmsDailySettlementLogVO(WmsDailySettlementLogVO wmsDailySettlementLogVO) {
		wmsDailySettlementLogManage.addWmsDailySettlementLogVO(wmsDailySettlementLogVO);
		return true;
	}

	public List<WmsDailySettlementLogVO> queryWmsDailySettlementLogVOList(WmsDailySettlementLogQueryVO wmsDailySettlementLogQueryVO) {
		return wmsDailySettlementLogManage.queryWmsDailySettlementLogVOList(wmsDailySettlementLogQueryVO);
	}

	public WmsDailySettlementLogVO getWmsDailySettlementLogVOByID(Long id) {
		return (WmsDailySettlementLogVO) wmsDailySettlementLogManage.getWmsDailySettlementLogVOByID(id);
	}

	public Boolean modifyWmsDailySettlementLogVO(WmsDailySettlementLogVO wmsDailySettlementLogVO) {
		return wmsDailySettlementLogManage.modifyWmsDailySettlementLogVO(wmsDailySettlementLogVO);
	}

	public Boolean deleteWmsDailySettlementLogVOByID(Long id) {
		return wmsDailySettlementLogManage.deleteWmsDailySettlementLogVOByID(id);
	}
}
