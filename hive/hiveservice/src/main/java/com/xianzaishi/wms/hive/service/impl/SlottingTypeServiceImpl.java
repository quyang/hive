package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.SlottingTypeVO;
import com.xianzaishi.wms.hive.vo.SlottingTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.ISlottingTypeService;
import com.xianzaishi.wms.hive.manage.itf.ISlottingTypeManage;

public class SlottingTypeServiceImpl implements ISlottingTypeService {
	@Autowired
	private ISlottingTypeManage slottingTypeManage = null;

	public ISlottingTypeManage getSlottingTypeManage() {
		return slottingTypeManage;
	}

	public void setSlottingTypeManage(ISlottingTypeManage slottingTypeManage) {
		this.slottingTypeManage = slottingTypeManage;
	}
	
	public Boolean addSlottingTypeVO(SlottingTypeVO slottingTypeVO) {
		slottingTypeManage.addSlottingTypeVO(slottingTypeVO);
		return true;
	}

	public List<SlottingTypeVO> querySlottingTypeVOList(SlottingTypeQueryVO slottingTypeQueryVO) {
		return slottingTypeManage.querySlottingTypeVOList(slottingTypeQueryVO);
	}

	public SlottingTypeVO getSlottingTypeVOByID(Long id) {
		return (SlottingTypeVO) slottingTypeManage.getSlottingTypeVOByID(id);
	}

	public Boolean modifySlottingTypeVO(SlottingTypeVO slottingTypeVO) {
		return slottingTypeManage.modifySlottingTypeVO(slottingTypeVO);
	}

	public Boolean deleteSlottingTypeVOByID(Long id) {
		return slottingTypeManage.deleteSlottingTypeVOByID(id);
	}
}
