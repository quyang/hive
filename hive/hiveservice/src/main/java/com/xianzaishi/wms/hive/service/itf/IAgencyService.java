package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.AgencyVO;
import com.xianzaishi.wms.hive.vo.AgencyQueryVO;

public interface IAgencyService {

	public Boolean addAgencyVO(AgencyVO agencyVO);

	public List<AgencyVO> queryAgencyVOList(AgencyQueryVO agencyQueryVO);

	public AgencyVO getAgencyVOByID(Long id);

	public Boolean modifyAgencyVO(AgencyVO agencyVO);

	public Boolean deleteAgencyVOByID(Long id);

}