package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsConfigDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsConfigManage;
import com.xianzaishi.wms.hive.vo.WmsConfigQueryVO;
import com.xianzaishi.wms.hive.vo.WmsConfigVO;

public class WmsConfigManageImpl implements IWmsConfigManage {
	@Autowired
	private IWmsConfigDao wmsConfigDao = null;

	private void validate(WmsConfigVO wmsConfigVO) {
		if (wmsConfigVO.getType() == null || wmsConfigVO.getType() <= 0) {
			throw new BizException("type error：" + wmsConfigVO.getType());
		}
		if (wmsConfigVO.getValue() == null || wmsConfigVO.getValue().isEmpty()) {
			throw new BizException("value error：" + wmsConfigVO.getValue());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsConfigDao getWmsConfigDao() {
		return wmsConfigDao;
	}

	public void setWmsConfigDao(IWmsConfigDao wmsConfigDao) {
		this.wmsConfigDao = wmsConfigDao;
	}

	public Boolean addWmsConfigVO(WmsConfigVO wmsConfigVO) {
		validate(wmsConfigVO);
		wmsConfigDao.addDO(wmsConfigVO);
		return true;
	}

	public List<WmsConfigVO> queryWmsConfigVOList(
			WmsConfigQueryVO wmsConfigQueryVO) {
		return wmsConfigDao.queryDO(wmsConfigQueryVO);
	}

	public WmsConfigVO getWmsConfigVOByID(Long id) {
		return (WmsConfigVO) wmsConfigDao.getDOByID(id);
	}

	public Boolean modifyWmsConfigVO(WmsConfigVO wmsConfigVO) {
		return wmsConfigDao.updateDO(wmsConfigVO);
	}

	public Boolean deleteWmsConfigVOByID(Long id) {
		return wmsConfigDao.delDO(id);
	}
}
