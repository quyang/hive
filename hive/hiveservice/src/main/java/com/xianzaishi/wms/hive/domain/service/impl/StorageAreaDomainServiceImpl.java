package com.xianzaishi.wms.hive.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.domain.client.itf.IBarcodeSequenceDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IStorageAreaDomainService;
import com.xianzaishi.wms.hive.service.itf.IStoryAreaService;
import com.xianzaishi.wms.hive.vo.StoryAreaQueryVO;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

public class StorageAreaDomainServiceImpl implements IStorageAreaDomainService {

	@Autowired
	private IBarcodeSequenceDomainClient barcodeSequenceDomainClient = null;
	@Autowired
	private IStoryAreaService storyAreaService = null;

	public Boolean addStorageArea(StoryAreaVO storyAreaVO) {
		return storyAreaService.addStoryAreaVO(storyAreaVO);
	}

	public IBarcodeSequenceDomainClient getBarcodeSequenceDomainClient() {
		return barcodeSequenceDomainClient;
	}

	public void setBarcodeSequenceDomainClient(
			IBarcodeSequenceDomainClient barcodeSequenceDomainClient) {
		this.barcodeSequenceDomainClient = barcodeSequenceDomainClient;
	}

	public IStoryAreaService getStoryAreaService() {
		return storyAreaService;
	}

	public void setStoryAreaService(IStoryAreaService storyAreaService) {
		this.storyAreaService = storyAreaService;
	}

	public StoryAreaVO getStorageAreaByID(Long id) {
		return storyAreaService.getStoryAreaVOByID(id);
	}

	public StoryAreaVO getStorageAreaByCode(Long agencyID, String code) {
		StoryAreaQueryVO storyAreaQueryVO = new StoryAreaQueryVO();
		storyAreaQueryVO.setAgencyId(agencyID);
		storyAreaQueryVO.setCode(code);
		List<StoryAreaVO> storyAreaVOs = storyAreaService
				.queryStoryAreaVOList(storyAreaQueryVO);
		if (storyAreaVOs != null && storyAreaVOs.size() > 0) {
			return storyAreaVOs.get(0);
		}
		return null;
	}
}
