package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogVO;
import com.xianzaishi.wms.hive.vo.WmsDailySettlementLogQueryVO;

public interface IWmsDailySettlementLogService {

	public Boolean addWmsDailySettlementLogVO(WmsDailySettlementLogVO wmsDailySettlementLogVO);

	public List<WmsDailySettlementLogVO> queryWmsDailySettlementLogVOList(WmsDailySettlementLogQueryVO wmsDailySettlementLogQueryVO);

	public WmsDailySettlementLogVO getWmsDailySettlementLogVOByID(Long id);

	public Boolean modifyWmsDailySettlementLogVO(WmsDailySettlementLogVO wmsDailySettlementLogVO);

	public Boolean deleteWmsDailySettlementLogVOByID(Long id);

}