package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsAlarmDao;

public class WmsAlarmDaoImpl extends BaseDaoAdapter implements IWmsAlarmDao {
	public String getVOClassName() {
		return "WmsAlarmDO";
	}
}
