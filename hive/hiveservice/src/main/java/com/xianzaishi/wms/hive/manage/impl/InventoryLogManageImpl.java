package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.InventoryLogVO;
import com.xianzaishi.wms.hive.vo.InventoryLogDO;
import com.xianzaishi.wms.hive.vo.InventoryLogQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.manage.itf.IInventoryLogManage;

import com.xianzaishi.wms.hive.dao.itf.IInventoryLogDao;

public class InventoryLogManageImpl implements IInventoryLogManage {
	@Autowired
	private IInventoryLogDao inventoryLogDao = null;

	private void validate(InventoryLogVO inventoryLogVO) {
		if (inventoryLogVO.getAgencyId() == null
				|| inventoryLogVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ inventoryLogVO.getAgencyId());
		}
		if (inventoryLogVO.getOperator() == null
				|| inventoryLogVO.getOperator() <= 0) {
			throw new BizException("operator error："
					+ inventoryLogVO.getOperator());
		}
		if (inventoryLogVO.getPositionId() == null
				|| inventoryLogVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ inventoryLogVO.getPositionId());
		}
		if (inventoryLogVO.getSkuId() == null || inventoryLogVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + inventoryLogVO.getSkuId());
		}
		if (inventoryLogVO.getOpNumber() == null) {
			throw new BizException("number error："
					+ inventoryLogVO.getOpNumber());
		}
		if (inventoryLogVO.getOpType() == null
				|| inventoryLogVO.getOpType() <= 0) {
			throw new BizException("opType error：" + inventoryLogVO.getOpType());
		}
		if (inventoryLogVO.getOpReason() == null
				|| inventoryLogVO.getOpReason() <= 0) {
			throw new BizException("opReason error："
					+ inventoryLogVO.getOpReason());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInventoryLogDao getInventoryLogDao() {
		return inventoryLogDao;
	}

	public void setInventoryLogDao(IInventoryLogDao inventoryLogDao) {
		this.inventoryLogDao = inventoryLogDao;
	}

	public Boolean addInventoryLogVO(InventoryLogVO inventoryLogVO) {
		validate(inventoryLogVO);
		inventoryLogDao.addDO(inventoryLogVO);
		return true;
	}

	public List<InventoryLogVO> queryInventoryLogVOList(
			InventoryLogQueryVO inventoryLogQueryVO) {
		return inventoryLogDao.queryDO(inventoryLogQueryVO);
	}

	public InventoryLogVO getInventoryLogVOByID(Long id) {
		return (InventoryLogVO) inventoryLogDao.getDOByID(id);
	}

	public Boolean modifyInventoryLogVO(InventoryLogVO inventoryLogVO) {
		return inventoryLogDao.updateDO(inventoryLogVO);
	}

	public Boolean deleteInventoryLogVOByID(Long id) {
		return inventoryLogDao.delDO(id);
	}
}
