package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.ISlottingTypeDao;
import com.xianzaishi.wms.hive.manage.itf.ISlottingTypeManage;
import com.xianzaishi.wms.hive.vo.SlottingTypeQueryVO;
import com.xianzaishi.wms.hive.vo.SlottingTypeVO;

public class SlottingTypeManageImpl implements ISlottingTypeManage {
	@Autowired
	private ISlottingTypeDao slottingTypeDao = null;

	private void validate(SlottingTypeVO slottingTypeVO) {
		if (slottingTypeVO.getName() == null
				|| slottingTypeVO.getName().isEmpty()) {
			throw new BizException("name error：" + slottingTypeVO.getName());
		}

	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ISlottingTypeDao getSlottingTypeDao() {
		return slottingTypeDao;
	}

	public void setSlottingTypeDao(ISlottingTypeDao slottingTypeDao) {
		this.slottingTypeDao = slottingTypeDao;
	}

	public Boolean addSlottingTypeVO(SlottingTypeVO slottingTypeVO) {
		validate(slottingTypeVO);
		slottingTypeDao.addDO(slottingTypeVO);
		return true;
	}

	public List<SlottingTypeVO> querySlottingTypeVOList(
			SlottingTypeQueryVO slottingTypeQueryVO) {
		return slottingTypeDao.queryDO(slottingTypeQueryVO);
	}

	public SlottingTypeVO getSlottingTypeVOByID(Long id) {
		return (SlottingTypeVO) slottingTypeDao.getDOByID(id);
	}

	public Boolean modifySlottingTypeVO(SlottingTypeVO slottingTypeVO) {
		return slottingTypeDao.updateDO(slottingTypeVO);
	}

	public Boolean deleteSlottingTypeVOByID(Long id) {
		return slottingTypeDao.delDO(id);
	}
}
