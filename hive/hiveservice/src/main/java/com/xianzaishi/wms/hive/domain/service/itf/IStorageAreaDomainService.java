package com.xianzaishi.wms.hive.domain.service.itf;

import com.xianzaishi.wms.hive.vo.StoryAreaVO;

public interface IStorageAreaDomainService {

	public Boolean addStorageArea(StoryAreaVO storyAreaVO);

	public StoryAreaVO getStorageAreaByID(Long id);

	public StoryAreaVO getStorageAreaByCode(Long agencyID, String code);

}
