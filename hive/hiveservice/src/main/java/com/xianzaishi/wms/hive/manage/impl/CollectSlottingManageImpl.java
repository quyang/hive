package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.CollectSlottingVO;
import com.xianzaishi.wms.hive.vo.CollectSlottingDO;
import com.xianzaishi.wms.hive.vo.CollectSlottingQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.manage.itf.ICollectSlottingManage;

import com.xianzaishi.wms.hive.dao.itf.ICollectSlottingDao;

public class CollectSlottingManageImpl implements ICollectSlottingManage {
	@Autowired
	private ICollectSlottingDao collectSlottingDao = null;

	private void validate(CollectSlottingDO collectSlottingDO) {
		if (collectSlottingDO.getAgencyId() == null
				|| collectSlottingDO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ collectSlottingDO.getAgencyId());
		}
		if (collectSlottingDO.getAreaId() == null
				|| collectSlottingDO.getAreaId() <= 0) {
			throw new BizException("areaID error："
					+ collectSlottingDO.getAreaId());
		}
		if (collectSlottingDO.getCode() == null
				|| collectSlottingDO.getCode().isEmpty()) {
			throw new BizException("code error：" + collectSlottingDO.getCode());
		}
		if (collectSlottingDO.getBarcode() == null
				|| collectSlottingDO.getBarcode().isEmpty()) {
			throw new BizException("barcode error："
					+ collectSlottingDO.getBarcode());
		}
		if (collectSlottingDO.getColumnNo() == null
				|| collectSlottingDO.getColumnNo() <= 0) {
			throw new BizException("column number error："
					+ collectSlottingDO.getColumnNo());
		}
		if (collectSlottingDO.getLineNo() == null
				|| collectSlottingDO.getLineNo() <= 0) {
			throw new BizException("line number error："
					+ collectSlottingDO.getLineNo());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ICollectSlottingDao getCollectSlottingDao() {
		return collectSlottingDao;
	}

	public void setCollectSlottingDao(ICollectSlottingDao collectSlottingDao) {
		this.collectSlottingDao = collectSlottingDao;
	}

	public Boolean addCollectSlottingVO(CollectSlottingVO collectSlottingVO) {
		collectSlottingDao.addDO(collectSlottingVO);
		return true;
	}

	public List<CollectSlottingVO> queryCollectSlottingVOList(
			CollectSlottingQueryVO collectSlottingQueryVO) {
		return collectSlottingDao.queryDO(collectSlottingQueryVO);
	}

	public CollectSlottingVO getCollectSlottingVOByID(Long id) {
		return (CollectSlottingVO) collectSlottingDao.getDOByID(id);
	}

	public Boolean modifyCollectSlottingVO(CollectSlottingVO collectSlottingVO) {
		return collectSlottingDao.updateDO(collectSlottingVO);
	}

	public Boolean deleteCollectSlottingVOByID(Long id) {
		return collectSlottingDao.delDO(id);
	}
}
