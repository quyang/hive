package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsConfigDao;

public class WmsConfigDaoImpl extends BaseDaoAdapter implements IWmsConfigDao {
	public String getVOClassName() {
		return "WmsConfigDO";
	}
}
