package com.xianzaishi.wms.hive.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;

public interface IInventoryOpReasonDomainService {

	public List<InventoryOpReasonVO> getInventoryOpReasonByOpType(Long opType);

}
