package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.AgencyRelationVO;
import com.xianzaishi.wms.hive.vo.AgencyRelationQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IAgencyRelationService;
import com.xianzaishi.wms.hive.manage.itf.IAgencyRelationManage;

public class AgencyRelationServiceImpl implements IAgencyRelationService {
	@Autowired
	private IAgencyRelationManage agencyRelationManage = null;

	public IAgencyRelationManage getAgencyRelationManage() {
		return agencyRelationManage;
	}

	public void setAgencyRelationManage(IAgencyRelationManage agencyRelationManage) {
		this.agencyRelationManage = agencyRelationManage;
	}
	
	public Boolean addAgencyRelationVO(AgencyRelationVO agencyRelationVO) {
		agencyRelationManage.addAgencyRelationVO(agencyRelationVO);
		return true;
	}

	public List<AgencyRelationVO> queryAgencyRelationVOList(AgencyRelationQueryVO agencyRelationQueryVO) {
		return agencyRelationManage.queryAgencyRelationVOList(agencyRelationQueryVO);
	}

	public AgencyRelationVO getAgencyRelationVOByID(Long id) {
		return (AgencyRelationVO) agencyRelationManage.getAgencyRelationVOByID(id);
	}

	public Boolean modifyAgencyRelationVO(AgencyRelationVO agencyRelationVO) {
		return agencyRelationManage.modifyAgencyRelationVO(agencyRelationVO);
	}

	public Boolean deleteAgencyRelationVOByID(Long id) {
		return agencyRelationManage.deleteAgencyRelationVOByID(id);
	}
}
