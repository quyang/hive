package com.xianzaishi.wms.hive.domain.service.impl;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryDomainService;
import com.xianzaishi.wms.hive.dto.WmsGuardBitDTO;
import com.xianzaishi.wms.hive.dto.WmsGuardBitDTO.GuardTypeContants;
import com.xianzaishi.wms.hive.service.itf.IAgencyRelationService;
import com.xianzaishi.wms.hive.service.itf.ICollectSlottingService;
import com.xianzaishi.wms.hive.service.itf.IInventoryService;
import com.xianzaishi.wms.hive.service.itf.IPositionDetailService;
import com.xianzaishi.wms.hive.service.itf.IPositionService;
import com.xianzaishi.wms.hive.service.itf.IStoryAreaService;
import com.xianzaishi.wms.hive.service.itf.IWmsGuardBitService;
import com.xianzaishi.wms.hive.vo.AgencyRelationQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitVO;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryDomainServiceImpl implements IInventoryDomainService {
	@Autowired
	private IInventoryService inventoryService = null;
	@Autowired
	private IAgencyRelationService agencyRelationService = null;
	@Autowired
	private ICollectSlottingService collectSlottingService = null;
	@Autowired
	private IStoryAreaService StoryAreaService = null;
	@Autowired
	private IPositionService positionService = null;
	@Autowired
	private IPositionDetailService positionDetailService = null;
	@Autowired
	private IWmsGuardBitService wmsGuardBitService = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;

	private static Integer DEFAULT_GUARD_BIT = 0;

	public static final Logger logger = Logger.getLogger(InventoryDomainServiceImpl.class);

	public List<InventoryVO> getInventoryBySKUID(Long agencyID, Long skuID) {
		InventoryQueryVO inventoryQueryVO = new InventoryQueryVO();
		inventoryQueryVO.setAgencyId(agencyID);
		inventoryQueryVO.setSkuId(skuID);
		List<InventoryVO> inventories = inventoryService
				.queryInventoryVOList(inventoryQueryVO);
		if (inventories != null && inventories.size() > 0) {
			for (InventoryVO inventoryVO : inventories) {
				inventoryVO
						.setNumberRealTotal(inventoryVO.getNumberReal() == null ? 0
								: inventories.get(0).getNumberReal());
			}
		}
		return inventories;
	}

	public InventoryVO getInventoryByPositionAndSku(Long positionID, Long skuID) {
		InventoryQueryVO inventoryQueryVO = new InventoryQueryVO();
		inventoryQueryVO.setPositionId(positionID);
		inventoryQueryVO.setSkuId(skuID);
		List<InventoryVO> inventories = inventoryService
				.queryInventoryVOList(inventoryQueryVO);
		if (inventories != null && inventories.size() > 0) {
			inventories.get(0).setNumberRealTotal(
					inventories.get(0).getNumberReal() == null ? 0
							: inventories.get(0).getNumberReal());
			return inventories.get(0);
		}
		return null;
	}

	public IInventoryService getInventoryService() {
		return inventoryService;
	}

	public void setInventoryService(IInventoryService inventoryService) {
		this.inventoryService = inventoryService;
	}

	public void syncInventory(Long agencyID,
			InventoryManageDetailVO inventoryManageDetailVO) {
		InventoryQueryVO inventoryqueryVO = new InventoryQueryVO();
		inventoryqueryVO.setPositionId(inventoryManageDetailVO.getPositionId());
		inventoryqueryVO.setSkuId(inventoryManageDetailVO.getSkuId());
		List<InventoryVO> inventoryVOs = inventoryService
				.queryInventoryVOList(inventoryqueryVO);
		InventoryVO inventoryVO = null;
		if (inventoryVOs != null && !inventoryVOs.isEmpty()) {
			inventoryVO = inventoryVOs.get(0);
			Integer remain = 0;
			if (inventoryVO.getSaleUnitId() != null
					&& !inventoryVO.getSaleUnitId().equals(
							inventoryManageDetailVO.getSaleUnitId())) {
				remain = inventoryVO.getNumberReal()
						+ inventoryManageDetailVO.getNumber()
						* inventoryManageDetailVO.getSpec() / 1000;
			} else {
				remain = inventoryVO.getNumberReal()
						+ inventoryManageDetailVO.getNumber();
			}
			if (remain != 0) {
				inventoryVO.setNumberReal(remain);
				inventoryService.modifyInventoryVO(inventoryVO);
			} else {
				inventoryService.deleteInventoryVOByID(inventoryVO.getId());
			}
		} else {
			PositionVO positionVO = positionService
					.getPositionVOByID(inventoryManageDetailVO.getPositionId());
			inventoryVO = new InventoryVO();
			inventoryVO.setAgencyId(agencyID);
			ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
					SkuQuery.querySkuById(inventoryManageDetailVO.getSkuId()))
					.getModule();
			inventoryVO.setSaleUnit(valueService.queryValueNameByValueId(
					itemCommoditySkuDTO.getSkuUnit()).getModule());
			inventoryVO
					.setSaleUnitId(new Long(itemCommoditySkuDTO.getSkuUnit()));
			inventoryVO.setSaleUnitType(inventoryManageDetailVO
					.getSaleUnitType());
			if (!new Long(itemCommoditySkuDTO.getSkuUnit())
					.equals(inventoryManageDetailVO.getSaleUnitId())) {
				inventoryVO.setNumberReal(inventoryManageDetailVO.getNumber()
						* inventoryManageDetailVO.getSpec() / 1000);
			} else {
				inventoryVO.setNumberReal(inventoryManageDetailVO.getNumber());
			}
			inventoryVO.setSkuName(itemCommoditySkuDTO.getTitle());
			inventoryVO.setPositionCode(inventoryManageDetailVO
					.getPositionCode());
			inventoryVO.setPositionId(inventoryManageDetailVO.getPositionId());
			inventoryVO.setSkuId(inventoryManageDetailVO.getSkuId());
			inventoryVO.setAreaId(positionVO.getAreaId());
			inventoryVO.setSlottingId(positionVO.getSlottingId());
			inventoryVO.setAreaType(positionVO.getAreaType());
			inventoryVO.setSlottingType(positionVO.getSlottingType());
			inventoryService.addInventoryVO(inventoryVO);
		}
	}

	public InventoryVO get2CInventoryBySKUID(Long agencyID, Long skuID) {
		InventoryVO inventoryVO = new InventoryVO();
		inventoryVO.setAgencyId(agencyID);
		inventoryVO.setSkuId(skuID);
		Integer number = inventoryService.get2CInventory(agencyID, skuID);
		inventoryVO.setNumberReal(number == null ? 0 : number);
		inventoryVO.setNumberRealTotal(number == null ? 0 : number);

		WmsGuardBitQueryVO wmsGuardBitQueryVO = new WmsGuardBitQueryVO();
		wmsGuardBitQueryVO.setAgencyId(agencyID);
		wmsGuardBitQueryVO.setSkuId(skuID);
		List<WmsGuardBitVO> wmsGuardBitVOs = wmsGuardBitService
				.queryWmsGuardBitVOList(wmsGuardBitQueryVO);

		int flag = DEFAULT_GUARD_BIT;
		if (wmsGuardBitVOs != null && wmsGuardBitVOs.size() > 0) {
			if (wmsGuardBitVOs.get(0).getGuardType() == null
					|| wmsGuardBitVOs.get(0).getGuardType() == 0) {
				flag = wmsGuardBitVOs.get(0).getGuardBit() == null ? flag
						: wmsGuardBitVOs.get(0).getGuardBit();
			} else {
				if (wmsGuardBitVOs.get(0).getTotalRecommond() != null
						&& wmsGuardBitVOs.get(0).getGuardBit() != null) {
					flag = wmsGuardBitVOs.get(0).getTotalRecommond()
							* wmsGuardBitVOs.get(0).getGuardBit() / 100;
				}
			}
		}
		inventoryVO.setMaySale(inventoryVO.getNumberReal() > (flag * 1000));
		inventoryVO.setNumberReal(inventoryVO.getNumberReal() - (flag * 1000));
		inventoryVO.setGuardBit(String.valueOf(flag));
		return inventoryVO;
	}

	public InventoryVO get2CInventoryByGovAndSKU(Long govID, Long skuID) {
		AgencyRelationQueryVO queryVO = new AgencyRelationQueryVO();
		queryVO.setGovId(govID);
		queryVO.setDr(Short.parseShort("0"));
		agencyRelationService.queryAgencyRelationVOList(queryVO);
		return get2CInventoryBySKUID(agencyRelationService
				.queryAgencyRelationVOList(queryVO).get(0).getAgencyId(), skuID);
	}

	public IAgencyRelationService getAgencyRelationService() {
		return agencyRelationService;
	}

	public void setAgencyRelationService(
			IAgencyRelationService agencyRelationService) {
		this.agencyRelationService = agencyRelationService;
	}

	public ICollectSlottingService getCollectSlottingService() {
		return collectSlottingService;
	}

	public void setCollectSlottingService(
			ICollectSlottingService collectSlottingService) {
		this.collectSlottingService = collectSlottingService;
	}

	public IStoryAreaService getStoryAreaService() {
		return StoryAreaService;
	}

	public void setStoryAreaService(IStoryAreaService storyAreaService) {
		StoryAreaService = storyAreaService;
	}

	public IPositionService getPositionService() {
		return positionService;
	}

	public void setPositionService(IPositionService positionService) {
		this.positionService = positionService;
	}

	public List<InventoryVO> getAllInventoryBySKUID(Long agencyID, Long skuID) {
		InventoryQueryVO inventoryQueryVO = new InventoryQueryVO();
		inventoryQueryVO.setAgencyId(agencyID);
		inventoryQueryVO.setSkuId(skuID);
		return inventoryService.queryInventoryVOList(inventoryQueryVO);
	}

	public List<InventoryVO> getInventoryByPositionID(Long agencyID,
			Long positionID,Integer pageSize,Integer pageNum) {
		InventoryQueryVO inventoryQueryVO = new InventoryQueryVO();
		inventoryQueryVO.setAgencyId(agencyID);
		inventoryQueryVO.setPositionId(positionID);
		if(null != pageSize && null != pageNum && pageNum >= 0 && pageSize > 0){
		  inventoryQueryVO.setPage(pageNum);
		  inventoryQueryVO.setSize(pageSize);
		}
		return inventoryService.queryInventoryVOList(inventoryQueryVO);
	}

	public InventoryVO getSkuStand(Long agencyID, Long skuID) {
		PositionDetailQueryVO positionDetailQueryVO = new PositionDetailQueryVO();
		positionDetailQueryVO.setAgencyId(agencyID);
		positionDetailQueryVO.setSkuId(skuID);
		List<PositionDetailVO> positionDetailVOs = positionDetailService
				.queryPositionDetailVOList(positionDetailQueryVO);
		if (positionDetailVOs != null && positionDetailVOs.size() > 0) {
			InventoryVO inventory = new InventoryVO();
			inventory.setAgencyId(agencyID);
			inventory.setSkuId(skuID);
			inventory.setPositionId(positionDetailVOs.get(0).getPositionId());
			inventory.setPositionCode(positionService.getPositionVOByID(
					positionDetailVOs.get(0).getPositionId()).getCode());
			return inventory;
		}
		return null;
	}

	@Override
	public Boolean setGuardInventoryBySkuId(Long skuId, String guradCount) {
		WmsGuardBitQueryVO wmsGuardBitQueryVO = new WmsGuardBitQueryVO();
		wmsGuardBitQueryVO.setSkuId(skuId);
		List<WmsGuardBitVO> wmsGuardBitVOS = wmsGuardBitService
				.queryWmsGuardBitVOList(wmsGuardBitQueryVO);

		//查询商品信息
		ItemCommoditySkuDTO itemCommoditySkuDTO = getItemCommoditySkuDTO(skuId);
		Boolean is = itemCommoditySkuDTO.getIsSteelyardSku();//是否是称重
		logger.error("是否是称重=" + is);
		if (!is && !StringUtils.isNumeric(guradCount)) {
			throw new BizException("标品不能输入小数数量");
		}

		if (CollectionUtils.isEmpty(wmsGuardBitVOS)) {
			WmsGuardBitDTO wmsGuardBitDTO = new WmsGuardBitDTO();
			wmsGuardBitDTO.setGuardBit(is ? multiply(guradCount.trim(), "1000").intValue()
					: Integer.parseInt(guradCount.trim()));
			wmsGuardBitDTO.setSkuId(skuId);
			wmsGuardBitDTO.setAgencyId(1L);
			wmsGuardBitDTO.setGuardType(GuardTypeContants.COUNT);
			wmsGuardBitDTO.setTotalRecommond(0);
			if (!wmsGuardBitService.addWmsGuardBitVO(wmsGuardBitDTO)) {
				throw new BizException("插入安全库存信息失败");
			}
			return true;
		}

		WmsGuardBitVO guardBitVO = wmsGuardBitVOS.get(0);
		guardBitVO.setGuardBit(is ? multiply(guradCount.trim(), "1000").intValue()
				: Integer.parseInt(guradCount.trim()));
		Boolean aBoolean = wmsGuardBitService.modifyWmsGuardBitVO(guardBitVO);
		if (!aBoolean) {
			throw new BizException("修改安全库存失败");
		}
		return true;
	}

	/**
	 * 插入安全库存信息
	 */
	@Override
	public Boolean insettGuardInventoryInfo(WmsGuardBitQueryVO data) {
		WmsGuardBitQueryVO wmsGuardBitQueryVO = new WmsGuardBitQueryVO();
		wmsGuardBitQueryVO.setSkuId(data.getSkuId());
		List<WmsGuardBitVO> wmsGuardBitVOS = wmsGuardBitService
				.queryWmsGuardBitVOList(wmsGuardBitQueryVO);
		if (CollectionUtils.isNotEmpty(wmsGuardBitVOS)) {
			throw new BizException("该sku对应的安全库存信息已存在");
		}

		WmsGuardBitVO wmsGuardBitVO = new WmsGuardBitVO();
		BeanCopierUtils.copyProperties(data, wmsGuardBitVO);
		Boolean aBoolean = wmsGuardBitService.addWmsGuardBitVO(wmsGuardBitVO);
		if (!aBoolean) {
			throw new BizException("添加安全库存信息失败");
		}
		return true;
	}

	public IPositionDetailService getPositionDetailService() {
		return positionDetailService;
	}

	public void setPositionDetailService(
			IPositionDetailService positionDetailService) {
		this.positionDetailService = positionDetailService;
	}

	public List<InventoryVO> batchGet2CInventoryBySKUID(Long agencyID,
			List<Long> skuIDs) {
		List<InventoryVO> inventoryVOs = inventoryService.batchGet2CInventory(
				agencyID, skuIDs);
		if (inventoryVOs != null) {
			for (InventoryVO inventoryVO : inventoryVOs) {
				inventoryVO.setAgencyId(agencyID);
				inventoryVO
						.setNumberReal(inventoryVO.getNumberReal() == null ? 0
								: inventoryVO.getNumberReal());
				inventoryVO
						.setNumberRealTotal(inventoryVO.getNumberReal() == null ? 0
								: inventoryVO.getNumberReal());

				WmsGuardBitQueryVO wmsGuardBitQueryVO = new WmsGuardBitQueryVO();
				wmsGuardBitQueryVO.setAgencyId(agencyID);
				wmsGuardBitQueryVO.setSkuId(inventoryVO.getSkuId());
				List<WmsGuardBitVO> wmsGuardBitVOs = wmsGuardBitService
						.queryWmsGuardBitVOList(wmsGuardBitQueryVO);

				int flag = DEFAULT_GUARD_BIT;
				if (wmsGuardBitVOs != null && wmsGuardBitVOs.size() > 0) {
					if (wmsGuardBitVOs.get(0).getGuardType() == null
							|| wmsGuardBitVOs.get(0).getGuardType() == 0) {
						flag = wmsGuardBitVOs.get(0).getGuardBit() == null ? flag
								: wmsGuardBitVOs.get(0).getGuardBit();
					} else {
						if (wmsGuardBitVOs.get(0).getTotalRecommond() != null
								&& wmsGuardBitVOs.get(0).getGuardBit() != null) {
							flag = wmsGuardBitVOs.get(0).getTotalRecommond()
									* wmsGuardBitVOs.get(0).getGuardBit() / 100;
						}
					}
				}
				inventoryVO
						.setMaySale(inventoryVO.getNumberReal() > (flag * 1000));
				inventoryVO.setNumberReal(inventoryVO.getNumberReal()
						- (flag * 1000));
				inventoryVO.setGuardBit(String.valueOf(flag));
			}
		}
		return inventoryVOs;
	}

	public List<InventoryVO> batchGet2CInventoryByGovAndSKU(Long govID,
			List<Long> skuIDs) {
		AgencyRelationQueryVO queryVO = new AgencyRelationQueryVO();
		queryVO.setGovId(govID);
		queryVO.setDr(Short.parseShort("0"));
		agencyRelationService.queryAgencyRelationVOList(queryVO);
		return batchGet2CInventoryBySKUID(agencyRelationService
				.queryAgencyRelationVOList(queryVO).get(0).getAgencyId(),
				skuIDs);
	}

	public IWmsGuardBitService getWmsGuardBitService() {
		return wmsGuardBitService;
	}

	public void setWmsGuardBitService(IWmsGuardBitService wmsGuardBitService) {
		this.wmsGuardBitService = wmsGuardBitService;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}


	/**
	 * 通过skuId查询商品信息
	 */
	public ItemCommoditySkuDTO getItemCommoditySkuDTO(Long skuId) {

		if (null == skuId || skuId < 0) {
			throw new BizException("skuId不正确");
		}
		//查询商品信息
		Result<ItemCommoditySkuDTO> commoditySkuDTOResult = skuService
				.queryItemSku(SkuQuery.querySkuById(skuId));
		if (null == commoditySkuDTOResult || !commoditySkuDTOResult.getSuccess()
				|| null == commoditySkuDTOResult.getModule()) {
			throw new BizException("skuId对应的商品信息不存在");
		}
		return commoditySkuDTOResult.getModule();
	}

	/**
	 * a 乘以b
	 * @param chengshu
	 * @param beiChengShu
	 * @return
	 */
	public BigDecimal multiply(String chengshu, String beiChengShu) {
		BigDecimal bigDecimal = new BigDecimal(chengshu);
		BigDecimal bigDecimal1 = new BigDecimal(beiChengShu);
		return bigDecimal.multiply(bigDecimal1);
	}

	/**
	 * a 加b
	 * @param jiashu
	 * @param beiJiaShu
	 * @return
	 */
	public  BigDecimal add(String jiashu, String beiJiaShu) {
		BigDecimal bigDecimal = new BigDecimal(jiashu);
		BigDecimal bigDecimal1 = new BigDecimal(beiJiaShu);
		return bigDecimal.add(bigDecimal1);
	}
}
