package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.StoryAreaVO;
import com.xianzaishi.wms.hive.vo.StoryAreaQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IStoryAreaService;
import com.xianzaishi.wms.hive.manage.itf.IStoryAreaManage;

public class StoryAreaServiceImpl implements IStoryAreaService {
	@Autowired
	private IStoryAreaManage storyAreaManage = null;

	public IStoryAreaManage getStoryAreaManage() {
		return storyAreaManage;
	}

	public void setStoryAreaManage(IStoryAreaManage storyAreaManage) {
		this.storyAreaManage = storyAreaManage;
	}
	
	public Boolean addStoryAreaVO(StoryAreaVO storyAreaVO) {
		storyAreaManage.addStoryAreaVO(storyAreaVO);
		return true;
	}

	public List<StoryAreaVO> queryStoryAreaVOList(StoryAreaQueryVO storyAreaQueryVO) {
		return storyAreaManage.queryStoryAreaVOList(storyAreaQueryVO);
	}

	public StoryAreaVO getStoryAreaVOByID(Long id) {
		return (StoryAreaVO) storyAreaManage.getStoryAreaVOByID(id);
	}

	public Boolean modifyStoryAreaVO(StoryAreaVO storyAreaVO) {
		return storyAreaManage.modifyStoryAreaVO(storyAreaVO);
	}

	public Boolean deleteStoryAreaVOByID(Long id) {
		return storyAreaManage.deleteStoryAreaVOByID(id);
	}
}
