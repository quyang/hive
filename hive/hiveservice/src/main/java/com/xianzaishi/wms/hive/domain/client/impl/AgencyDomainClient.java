package com.xianzaishi.wms.hive.domain.client.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IAgencyDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IAgencyDomainService;
import com.xianzaishi.wms.hive.vo.AgencyVO;

public class AgencyDomainClient implements IAgencyDomainClient {
	@Autowired
	private IAgencyDomainService agencyDomainService = null;

	public SimpleResultVO<AgencyVO> getAgencyByID(Long agencyID) {
		return SimpleResultVO.buildSuccessResult(agencyDomainService
				.getAgencyByID(agencyID));
	}

	public IAgencyDomainService getAgencyDomainService() {
		return agencyDomainService;
	}

	public void setAgencyDomainService(IAgencyDomainService agencyDomainService) {
		this.agencyDomainService = agencyDomainService;
	}
}
