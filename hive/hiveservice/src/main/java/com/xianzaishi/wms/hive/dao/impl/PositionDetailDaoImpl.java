package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IPositionDetailDao;

public class PositionDetailDaoImpl extends BaseDaoAdapter implements
		IPositionDetailDao {
	public String getVOClassName() {
		return "PositionDetailDO";
	}
}
