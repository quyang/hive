package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.WmsAgencyConfigVO;
import com.xianzaishi.wms.hive.vo.WmsAgencyConfigQueryVO;

public interface IWmsAgencyConfigService {

	public Boolean addWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO);

	public List<WmsAgencyConfigVO> queryWmsAgencyConfigVOList(WmsAgencyConfigQueryVO wmsAgencyConfigQueryVO);

	public WmsAgencyConfigVO getWmsAgencyConfigVOByID(Long id);

	public Boolean modifyWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO);

	public Boolean deleteWmsAgencyConfigVOByID(Long id);

}