package com.xianzaishi.wms.hive.service.itf;

import java.util.List;

import com.xianzaishi.wms.hive.vo.InventoryLogVO;
import com.xianzaishi.wms.hive.vo.InventoryLogQueryVO;

public interface IInventoryLogService {

	public Boolean addInventoryLogVO(InventoryLogVO inventoryLogVO);

	public List<InventoryLogVO> queryInventoryLogVOList(InventoryLogQueryVO inventoryLogQueryVO);

	public InventoryLogVO getInventoryLogVOByID(Long id);

	public Boolean modifyInventoryLogVO(InventoryLogVO inventoryLogVO);

	public Boolean deleteInventoryLogVOByID(Long id);

}