package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsAgencyConfigDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsAgencyConfigManage;
import com.xianzaishi.wms.hive.vo.WmsAgencyConfigQueryVO;
import com.xianzaishi.wms.hive.vo.WmsAgencyConfigVO;

public class WmsAgencyConfigManageImpl implements IWmsAgencyConfigManage {
	@Autowired
	private IWmsAgencyConfigDao wmsAgencyConfigDao = null;

	private void validate(WmsAgencyConfigVO wmsAgencyConfigVO) {
		if (wmsAgencyConfigVO.getAgencyId() == null
				|| wmsAgencyConfigVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ wmsAgencyConfigVO.getAgencyId());
		}
		if (wmsAgencyConfigVO.getType() == null
				|| wmsAgencyConfigVO.getType() <= 0) {
			throw new BizException("type error：" + wmsAgencyConfigVO.getType());
		}
		if (wmsAgencyConfigVO.getValue() == null
				|| wmsAgencyConfigVO.getValue().isEmpty()) {
			throw new BizException("name error：" + wmsAgencyConfigVO.getValue());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsAgencyConfigDao getWmsAgencyConfigDao() {
		return wmsAgencyConfigDao;
	}

	public void setWmsAgencyConfigDao(IWmsAgencyConfigDao wmsAgencyConfigDao) {
		this.wmsAgencyConfigDao = wmsAgencyConfigDao;
	}

	public Boolean addWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO) {
		validate(wmsAgencyConfigVO);
		wmsAgencyConfigDao.addDO(wmsAgencyConfigVO);
		return true;
	}

	public List<WmsAgencyConfigVO> queryWmsAgencyConfigVOList(
			WmsAgencyConfigQueryVO wmsAgencyConfigQueryVO) {
		return wmsAgencyConfigDao.queryDO(wmsAgencyConfigQueryVO);
	}

	public WmsAgencyConfigVO getWmsAgencyConfigVOByID(Long id) {
		return (WmsAgencyConfigVO) wmsAgencyConfigDao.getDOByID(id);
	}

	public Boolean modifyWmsAgencyConfigVO(WmsAgencyConfigVO wmsAgencyConfigVO) {
		return wmsAgencyConfigDao.updateDO(wmsAgencyConfigVO);
	}

	public Boolean deleteWmsAgencyConfigVOByID(Long id) {
		return wmsAgencyConfigDao.delDO(id);
	}
}
