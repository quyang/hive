package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IStorageAreaTypeDao;

public class StorageAreaTypeDaoImpl extends BaseDaoAdapter implements
		IStorageAreaTypeDao {
	public String getVOClassName() {
		return "StorageAreaTypeDO";
	}
}
