package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IWmsConfigTypeDao;
import com.xianzaishi.wms.hive.manage.itf.IWmsConfigTypeManage;
import com.xianzaishi.wms.hive.vo.WmsConfigTypeQueryVO;
import com.xianzaishi.wms.hive.vo.WmsConfigTypeVO;

public class WmsConfigTypeManageImpl implements IWmsConfigTypeManage {
	@Autowired
	private IWmsConfigTypeDao wmsConfigTypeDao = null;

	private void validate(WmsConfigTypeVO wmsConfigTypeVO) {
		if (wmsConfigTypeVO.getName() == null
				|| wmsConfigTypeVO.getName().isEmpty()) {
			throw new BizException("name error：" + wmsConfigTypeVO.getName());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IWmsConfigTypeDao getWmsConfigTypeDao() {
		return wmsConfigTypeDao;
	}

	public void setWmsConfigTypeDao(IWmsConfigTypeDao wmsConfigTypeDao) {
		this.wmsConfigTypeDao = wmsConfigTypeDao;
	}

	public Boolean addWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO) {
		validate(wmsConfigTypeVO);
		wmsConfigTypeDao.addDO(wmsConfigTypeVO);
		return true;
	}

	public List<WmsConfigTypeVO> queryWmsConfigTypeVOList(
			WmsConfigTypeQueryVO wmsConfigTypeQueryVO) {
		return wmsConfigTypeDao.queryDO(wmsConfigTypeQueryVO);
	}

	public WmsConfigTypeVO getWmsConfigTypeVOByID(Long id) {
		return (WmsConfigTypeVO) wmsConfigTypeDao.getDOByID(id);
	}

	public Boolean modifyWmsConfigTypeVO(WmsConfigTypeVO wmsConfigTypeVO) {
		return wmsConfigTypeDao.updateDO(wmsConfigTypeVO);
	}

	public Boolean deleteWmsConfigTypeVOByID(Long id) {
		return wmsConfigTypeDao.delDO(id);
	}
}
