package com.xianzaishi.wms.hive.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IPositionDomainService;
import com.xianzaishi.wms.hive.vo.PositionVO;

public class PositionDomainClient implements IPositionDomainClient {
	private static final Logger logger = Logger
			.getLogger(PositionDomainClient.class);

	@Autowired
	private IPositionDomainService positionDomainService = null;

	public SimpleResultVO<PositionVO> getPositonByBarcode(String barcode) {
		SimpleResultVO<PositionVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(positionDomainService
					.getPositonByBarcode(barcode));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public IPositionDomainService getPositionDomainService() {
		return positionDomainService;
	}

	public void setPositionDomainService(
			IPositionDomainService positionDomainService) {
		this.positionDomainService = positionDomainService;
	}

	public SimpleResultVO<Boolean> addPosition(PositionVO positionVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			positionDomainService.addPosition(positionVO);
			flag = SimpleResultVO.buildSuccessResult(true);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<PositionVO> getPositionVOByID(Long id) {
		SimpleResultVO<PositionVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(positionDomainService
					.getPositionVOByID(id));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<List<PositionVO>> batchGetPositionVOByBarcode(
			List<String> barcodes) {
		SimpleResultVO<List<PositionVO>> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(positionDomainService
					.batchGetPositionVOByBarcode(barcodes));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<List<PositionVO>> batchGetPositionVOByID(
			List<Long> ids) {
		SimpleResultVO<List<PositionVO>> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(positionDomainService
					.batchGetPositionVOByID(ids));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	@Override
	public SimpleResultVO<PositionVO> getPositonByCode(Long agencyID,
			String code) {
		SimpleResultVO<PositionVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(positionDomainService
					.getPositonByCode(agencyID, code));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}