package com.xianzaishi.wms.hive.domain.service.itf;

import com.xianzaishi.wms.hive.vo.InventoryManageVO;

public interface IInventoryManageDomainService {

	// domain
	public Boolean createInventoryManageDomain(
			InventoryManageVO inventoryManageVO);

	public InventoryManageVO getInventoryManageDomainByID(Long inventoryManageID);

	public void syncInventory(InventoryManageVO inventoryManageVO);

	/**
	 * 更新流水信息，包括子流水信息
	 * @param inventoryManageVO
	 * @return
	 */
	public Boolean updateInventoryManageDomain(InventoryManageVO inventoryManageVO);
}
