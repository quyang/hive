package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.WmsGuardBitVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IWmsGuardBitService;
import com.xianzaishi.wms.hive.manage.itf.IWmsGuardBitManage;

public class WmsGuardBitServiceImpl implements IWmsGuardBitService {
	@Autowired
	private IWmsGuardBitManage wmsGuardBitManage = null;

	public IWmsGuardBitManage getWmsGuardBitManage() {
		return wmsGuardBitManage;
	}

	public void setWmsGuardBitManage(IWmsGuardBitManage wmsGuardBitManage) {
		this.wmsGuardBitManage = wmsGuardBitManage;
	}
	
	public Boolean addWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO) {
		wmsGuardBitManage.addWmsGuardBitVO(wmsGuardBitVO);
		return true;
	}

	public List<WmsGuardBitVO> queryWmsGuardBitVOList(WmsGuardBitQueryVO wmsGuardBitQueryVO) {
		return wmsGuardBitManage.queryWmsGuardBitVOList(wmsGuardBitQueryVO);
	}

	public WmsGuardBitVO getWmsGuardBitVOByID(Long id) {
		return (WmsGuardBitVO) wmsGuardBitManage.getWmsGuardBitVOByID(id);
	}

	public Boolean modifyWmsGuardBitVO(WmsGuardBitVO wmsGuardBitVO) {
		return wmsGuardBitManage.modifyWmsGuardBitVO(wmsGuardBitVO);
	}

	public Boolean deleteWmsGuardBitVOByID(Long id) {
		return wmsGuardBitManage.deleteWmsGuardBitVOByID(id);
	}
}
