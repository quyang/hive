package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IPositionDetailDao;
import com.xianzaishi.wms.hive.manage.itf.IPositionDetailManage;
import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public class PositionDetailManageImpl implements IPositionDetailManage {
	@Autowired
	private IPositionDetailDao positionDetailDao = null;

	private void validate(PositionDetailVO positionDetailVO) {
		if (positionDetailVO.getAgencyId() == null
				|| positionDetailVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ positionDetailVO.getAgencyId());
		}
		if (positionDetailVO.getPositionId() == null
				|| positionDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ positionDetailVO.getPositionId());
		}
		if (positionDetailVO.getSkuId() == null
				|| positionDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + positionDetailVO.getSkuId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IPositionDetailDao getPositionDetailDao() {
		return positionDetailDao;
	}

	public void setPositionDetailDao(IPositionDetailDao positionDetailDao) {
		this.positionDetailDao = positionDetailDao;
	}

	public Long addPositionDetailVO(PositionDetailVO positionDetailVO) {
		validate(positionDetailVO);
		return (Long) positionDetailDao.addDO(positionDetailVO);
	}

	public List<PositionDetailVO> queryPositionDetailVOList(
			PositionDetailQueryVO positionDetailQueryVO) {
		return positionDetailDao.queryDO(positionDetailQueryVO);
	}

	public PositionDetailVO getPositionDetailVOByID(Long id) {
		return (PositionDetailVO) positionDetailDao.getDOByID(id);
	}

	public Boolean modifyPositionDetailVO(PositionDetailVO positionDetailVO) {
		return positionDetailDao.updateDO(positionDetailVO);
	}

	public Boolean deletePositionDetailVOByID(Long id) {
		return positionDetailDao.delDO(id);
	}
}
