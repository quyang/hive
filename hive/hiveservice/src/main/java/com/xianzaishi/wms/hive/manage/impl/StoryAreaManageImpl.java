package com.xianzaishi.wms.hive.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.dao.itf.IStoryAreaDao;
import com.xianzaishi.wms.hive.manage.itf.IStoryAreaManage;
import com.xianzaishi.wms.hive.vo.StoryAreaQueryVO;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;

public class StoryAreaManageImpl implements IStoryAreaManage {
	@Autowired
	private IStoryAreaDao storyAreaDao = null;

	private void validate(StoryAreaVO storyAreaVO) {
		if (storyAreaVO.getAgencyId() == null || storyAreaVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ storyAreaVO.getAgencyId());
		}
		if (storyAreaVO.getAreaType() == null || storyAreaVO.getAreaType() <= 0) {
			throw new BizException("areaType error："
					+ storyAreaVO.getAreaType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStoryAreaDao getStoryAreaDao() {
		return storyAreaDao;
	}

	public void setStoryAreaDao(IStoryAreaDao storyAreaDao) {
		this.storyAreaDao = storyAreaDao;
	}

	public Boolean addStoryAreaVO(StoryAreaVO storyAreaVO) {
		validate(storyAreaVO);
		storyAreaDao.addDO(storyAreaVO);
		return true;
	}

	public List<StoryAreaVO> queryStoryAreaVOList(
			StoryAreaQueryVO storyAreaQueryVO) {
		return storyAreaDao.queryDO(storyAreaQueryVO);
	}

	public StoryAreaVO getStoryAreaVOByID(Long id) {
		return (StoryAreaVO) storyAreaDao.getDOByID(id);
	}

	public Boolean modifyStoryAreaVO(StoryAreaVO storyAreaVO) {
		return storyAreaDao.updateDO(storyAreaVO);
	}

	public Boolean deleteStoryAreaVOByID(Long id) {
		return storyAreaDao.delDO(id);
	}
}
