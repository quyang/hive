package com.xianzaishi.wms.hive.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.domain.service.itf.IInventoryConfigDomainService;
import com.xianzaishi.wms.hive.service.itf.IPositionDetailService;
import com.xianzaishi.wms.hive.vo.PositionDetailQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;

public class InventoryConfigDomainServiceImpl implements
		IInventoryConfigDomainService {
	@Autowired
	private IPositionDetailService positionDetailService = null;

	public Long addPositionDetail(PositionDetailVO positionDetailVO) {
		return positionDetailService.addPositionDetailVO(positionDetailVO);
	}

	public List<PositionDetailVO> getPositionDetailBySkuID(Long agencyID,
			Long skuID) {
		PositionDetailQueryVO queryVO = new PositionDetailQueryVO();
		queryVO.setAgencyId(agencyID);
		queryVO.setSkuId(skuID);
		return positionDetailService.queryPositionDetailVOList(queryVO);
	}

	public List<PositionDetailVO> getPositionDetailByPositionID(Long agencyID,
			Long positionID) {
		PositionDetailQueryVO queryVO = new PositionDetailQueryVO();
		queryVO.setAgencyId(agencyID);
		queryVO.setPositionId(positionID);
		return positionDetailService.queryPositionDetailVOList(queryVO);
	}

	public IPositionDetailService getPositionDetailService() {
		return positionDetailService;
	}

	public void setPositionDetailService(
			IPositionDetailService positionDetailService) {
		this.positionDetailService = positionDetailService;
	}

	public Boolean delPositionDetail(Long id) {
		return positionDetailService.deletePositionDetailVOByID(id);
	}
}
