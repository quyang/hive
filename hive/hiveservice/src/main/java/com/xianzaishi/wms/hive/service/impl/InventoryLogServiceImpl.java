package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.InventoryLogVO;
import com.xianzaishi.wms.hive.vo.InventoryLogQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IInventoryLogService;
import com.xianzaishi.wms.hive.manage.itf.IInventoryLogManage;

public class InventoryLogServiceImpl implements IInventoryLogService {
	@Autowired
	private IInventoryLogManage inventoryLogManage = null;

	public IInventoryLogManage getInventoryLogManage() {
		return inventoryLogManage;
	}

	public void setInventoryLogManage(IInventoryLogManage inventoryLogManage) {
		this.inventoryLogManage = inventoryLogManage;
	}
	
	public Boolean addInventoryLogVO(InventoryLogVO inventoryLogVO) {
		inventoryLogManage.addInventoryLogVO(inventoryLogVO);
		return true;
	}

	public List<InventoryLogVO> queryInventoryLogVOList(InventoryLogQueryVO inventoryLogQueryVO) {
		return inventoryLogManage.queryInventoryLogVOList(inventoryLogQueryVO);
	}

	public InventoryLogVO getInventoryLogVOByID(Long id) {
		return (InventoryLogVO) inventoryLogManage.getInventoryLogVOByID(id);
	}

	public Boolean modifyInventoryLogVO(InventoryLogVO inventoryLogVO) {
		return inventoryLogManage.modifyInventoryLogVO(inventoryLogVO);
	}

	public Boolean deleteInventoryLogVOByID(Long id) {
		return inventoryLogManage.deleteInventoryLogVOByID(id);
	}
}
