package com.xianzaishi.wms.hive.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.hive.dao.itf.IWmsAgencyConfigDao;

public class WmsAgencyConfigDaoImpl extends BaseDaoAdapter implements
		IWmsAgencyConfigDao {
	public String getVOClassName() {
		return "WmsAgencyConfigDO";
	}
}
