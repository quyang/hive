package com.xianzaishi.wms.hive.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.hive.vo.InventoryOpReasonVO;
import com.xianzaishi.wms.hive.vo.InventoryOpReasonQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.hive.service.itf.IInventoryOpReasonService;
import com.xianzaishi.wms.hive.manage.itf.IInventoryOpReasonManage;

public class InventoryOpReasonServiceImpl implements IInventoryOpReasonService {
	@Autowired
	private IInventoryOpReasonManage inventoryOpReasonManage = null;

	public IInventoryOpReasonManage getInventoryOpReasonManage() {
		return inventoryOpReasonManage;
	}

	public void setInventoryOpReasonManage(IInventoryOpReasonManage inventoryOpReasonManage) {
		this.inventoryOpReasonManage = inventoryOpReasonManage;
	}
	
	public Boolean addInventoryOpReasonVO(InventoryOpReasonVO inventoryOpReasonVO) {
		inventoryOpReasonManage.addInventoryOpReasonVO(inventoryOpReasonVO);
		return true;
	}

	public List<InventoryOpReasonVO> queryInventoryOpReasonVOList(InventoryOpReasonQueryVO inventoryOpReasonQueryVO) {
		return inventoryOpReasonManage.queryInventoryOpReasonVOList(inventoryOpReasonQueryVO);
	}

	public InventoryOpReasonVO getInventoryOpReasonVOByID(Long id) {
		return (InventoryOpReasonVO) inventoryOpReasonManage.getInventoryOpReasonVOByID(id);
	}

	public Boolean modifyInventoryOpReasonVO(InventoryOpReasonVO inventoryOpReasonVO) {
		return inventoryOpReasonManage.modifyInventoryOpReasonVO(inventoryOpReasonVO);
	}

	public Boolean deleteInventoryOpReasonVOByID(Long id) {
		return inventoryOpReasonManage.deleteInventoryOpReasonVOByID(id);
	}
}
