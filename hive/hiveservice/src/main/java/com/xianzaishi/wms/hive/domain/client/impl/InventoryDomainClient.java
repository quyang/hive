package com.xianzaishi.wms.hive.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.domain.service.itf.IInventoryDomainService;
import com.xianzaishi.wms.hive.vo.InventoryVO;

public class InventoryDomainClient implements IInventoryDomainClient {
	private static final Logger logger = Logger
			.getLogger(InventoryDomainClient.class);
	@Autowired
	private IInventoryDomainService inventoryDomainService = null;

	public SimpleResultVO<List<InventoryVO>> getInventoryBySKUID(Long agencyID,
			Long skuID) {
		SimpleResultVO<List<InventoryVO>> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inventoryDomainService
					.getInventoryBySKUID(agencyID, skuID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<InventoryVO> getInventoryByPositionAndSku(
			Long positionID, Long skuID) {
		SimpleResultVO<InventoryVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inventoryDomainService
					.getInventoryByPositionAndSku(positionID, skuID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public IInventoryDomainService getInventoryDomainService() {
		return inventoryDomainService;
	}

	public void setInventoryDomainService(
			IInventoryDomainService inventoryDomainService) {
		this.inventoryDomainService = inventoryDomainService;
	}

	public SimpleResultVO<Boolean> syncInventory(Long agencyID,
			Long positionID, Long skuID, Integer number) {
		return null;
	}

	public SimpleResultVO<List<InventoryVO>> getAllInventoryBySKUID(
			Long agencyID, Long skuID) {
		SimpleResultVO<List<InventoryVO>> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inventoryDomainService
					.getAllInventoryBySKUID(agencyID, skuID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<List<InventoryVO>> getInventoryByPositionID(
			Long agencyID, Long positionID,Integer pageSize,Integer pageNum) {
		SimpleResultVO<List<InventoryVO>> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inventoryDomainService
					.getInventoryByPositionID(agencyID, positionID,pageSize,pageNum));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<InventoryVO> getSkuStand(Long agencyID, Long skuID) {
		SimpleResultVO<InventoryVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inventoryDomainService
					.getSkuStand(agencyID, skuID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}
